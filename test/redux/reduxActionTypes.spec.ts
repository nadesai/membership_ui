import { expect } from 'chai'
import { actions, Actions, Docs } from '../../src/js/util/reduxActionTypes'

describe('reduxActionTypes.actions', () => {
  it('adds the prefix to the root key name', () => {
    const result = actions('prefix', {
      example: ''
    })
    expect(result).to.deep.equal({
      example: 'prefix.example'
    })
  })

  it('adds nested prefixes to the child key names', () => {
    const result = actions('prefix', {
      parent: {
        child1: '',
        child2: ''
      },
      example: ''
    })
    expect(result).to.deep.equal({
      parent: {
        child1: 'prefix.parent.child1',
        child2: 'prefix.parent.child2'
      },
      example: 'prefix.example'
    })
  })

  it('ignores defined string values (comments)', () => {
    const result = actions('prefix', {
      example: 'ignored comment'
    })
    expect(result).to.deep.equal({
      example: 'prefix.example'
    })
  })
})

describe('reduxActionTypes.Actions', () => {
  it('adds the prefix to the root key name', () => {
    const { prefix } = Actions({
      prefix: {
        example: ''
      }
    })
    expect(prefix.example).to.equal('prefix.example')
  })

  it('adds docs', () => {
    const documentExample = 'documentation'
    const root = Actions({
      prefix: {
        example: documentExample
      }
    })
    const docs = Docs(root)
    const { prefix } = root
    expect(docs(prefix.example)).to.equal(documentExample)
  })

  it('adds multiple roots to the key names', () => {
    const root = Actions({
      parent1: {
        child: 'child of parent1'
      },
      parent2: {
        child: 'child of parent2'
      }
    })
    const docs = Docs(root)
    const { parent1, parent2 } = root

    expect(docs('parent1.child')).to.equal('child of parent1')
    expect(parent1.child).to.equal('parent1.child')
    expect(parent2.child).to.equal('parent2.child')
  })
})
