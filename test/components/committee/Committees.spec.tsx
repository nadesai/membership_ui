import React from 'react'
import { dev } from '../../../src/js/redux/store/configureStore'
import { serviceLocator } from '../../../src/js/util/serviceLocator'
import Committees from '../../../src/js/components/committee/Committees'

import { expect } from 'chai'
import renderer from 'react-test-renderer'
import { fromJS } from 'immutable'

const store = dev({
  member: fromJS({
    isAdmin: true
  })
})
serviceLocator.store = store

describe('<Committees />', () => {
  xit('works', done => {
    const tree = renderer.create(<Committees store={store} />).toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
})
