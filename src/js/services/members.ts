import { List, Set } from 'immutable'
import { ImMemberState } from '../redux/reducers/memberReducers'

export function isMemberLoaded(member: ImMemberState) {
  return member && member.getIn(['user', 'data'], List()) != null
}

export function isMember(member: ImMemberState) {
  if (!member) {
    return false
  }
  return member
    .getIn(['user', 'data', 'roles'], List())
    .some(
      role =>
        role.get('role') === 'member' && role.get('committee') === 'general'
    )
}

export function isAdmin(member: ImMemberState) {
  if (!member) {
    return false
  }
  return member
    .getIn(['user', 'data', 'roles'], List())
    .some(
      role =>
        role.get('role') === 'admin' && role.get('committee') === 'general'
    )
}

export function isCommitteeAdmin(
  member: ImMemberState,
  committees: string | string[] = 'any'
) {
  if (!member || !committees) {
    return false
  }
  const isAdmin = role => role.get('role') === 'admin'
  const committeeSet =
    typeof committees === 'string' ? Set([committees]) : Set(committees)
  const filter =
    committees === 'any'
      ? isAdmin
      : role => isAdmin(role) && committeeSet.has(role.get('committee'))

  const memberRoles = member.getIn(['user', 'data', 'roles'], List())
  return memberRoles.some(filter)
}

const members = {
  isMemberLoaded,
  isAdmin,
  isCommitteeAdmin
}

export default members
