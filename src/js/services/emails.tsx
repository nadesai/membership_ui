import React from 'react'
import { c } from '../config'

export function adminEmail() {
  return <a href={`mailto:${c('ADMIN_EMAIL')}`}>{c('ADMIN_EMAIL')}</a>
}

export function eligibilityEmail() {
  return (
    <a href={`mailto:${c('ELIGIBILITY_EMAIL')}`}>{c('ELIGIBILITY_EMAIL')}</a>
  )
}
