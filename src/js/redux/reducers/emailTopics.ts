import { fromJS, Map } from 'immutable'
import { showNotification } from '../../util/util'
import * as types from './../constants/actionTypes'
import { AnyAction } from 'redux'
import { Reducer } from 'react'
import { EmailTopicById } from 'src/js/client/EmailTopicClient'
import { FromJS } from 'src/js/util/typedMap'

interface TopicsState {
  byId: EmailTopicById
}

export type ImTopicsState = FromJS<TopicsState>
export type TopicsAction = AnyAction

const INITIAL_STATE: ImTopicsState = fromJS({
  byId: {}
})

const topics: Reducer<ImTopicsState, TopicsAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case types.FETCH_EMAIL_TOPICS:
      return state.set(
        'byId',
        action.payload
          .toMap()
          .mapEntries(([_, topic]) => [topic.get('id'), topic])
      )
    case types.SEND_EMAIL_TEMPLATE_SUCCEEDED:
      return state
    case types.SEND_EMAIL_TEMPLATE_FAILED:
      return state
    default:
      return state
  }
}

export default topics
