import { fromJS, Map } from 'immutable'
import { EMAIL } from './../constants/actionTypes'
import { AnyAction } from 'redux'
import { Reducer } from 'react'
import { FromJS } from 'src/js/util/typedMap'
import { EmailById } from 'src/js/client/EmailAddressClient'

interface EmailsState {
  byId: EmailById
}

export type ImEmailsState = FromJS<EmailsState>
export type EmailsAction = AnyAction

const INITIAL_STATE: ImEmailsState = fromJS({
  byId: {}
})
const STATUS_NEW = 'new'
const NEW_EMAIL_RULE = fromJS({
  status: STATUS_NEW,
  email_address: '',
  forwarding_addresses: []
})

const emails: Reducer<ImEmailsState, EmailsAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case EMAIL.RULES.UPDATE_LIST:
      return addEmailRulesById(state, action.payload)
    case EMAIL.RULES.SAVE.SUCCESS:
      const email = action.email
      if (action.new_record) {
        state = state.deleteIn(['byId', 'new'])
      }
      return state.setIn(['byId', email.get('id')], email)
    case EMAIL.RULES.CREATE:
      return state.setIn(['byId', 'new'], NEW_EMAIL_RULE)
    case EMAIL.RULES.UPDATE_ADDRESS:
      return state.updateIn(['byId', action.id], record =>
        record.merge({
          status:
            record.get('status', null) === STATUS_NEW ? STATUS_NEW : 'modified',
          email_address: action.email_address
        })
      )
    case EMAIL.RULES.DELETE.SUCCESS:
      return state.deleteIn(['byId', action.id])
    case EMAIL.FORWARD.ADD: {
      const modified = state.updateIn(
        ['byId', action.id, 'status'],
        (status = 'modified') => status
      )
      return modified.updateIn(
        ['byId', action.id, 'forwarding_addresses'],
        list => list.push('')
      )
    }
    case EMAIL.FORWARD.REMOVE: {
      const modified = state.updateIn(
        ['byId', action.id, 'status'],
        (status = 'modified') => status
      )
      return modified.updateIn(
        ['byId', action.id, 'forwarding_addresses'],
        list => list.delete(action.forwardingIndex)
      )
    }
    case EMAIL.FORWARD.UPDATE: {
      const modified = state.updateIn(
        ['byId', action.id, 'status'],
        (status = 'modified') => status
      )
      return modified.setIn(
        ['byId', action.id, 'forwarding_addresses', action.forwardingIndex],
        action.forwardingAddress
      )
    }
    default:
      return state
  }
}

function addEmailRulesById(state: ImEmailsState, emailRules) {
  const rulesWithCodeClaimed = emailRules
    .toMap()
    .mapEntries(([_, m]) => [m.get('id'), m])
  return state.mergeDeepIn(['byId'], rulesWithCodeClaimed)
}

export default emails
