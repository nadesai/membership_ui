import * as types from './../constants/actionTypes'
import { EmailTopics } from '../../client/EmailTopicClient'
import { TODOAsyncThunkAction } from 'src/js/typeMigrationShims'
import { logError, showNotification } from 'src/js/util/util'

export function fetchEmailTopics(): TODOAsyncThunkAction {
  return async dispatch => {
    const topics = await EmailTopics.all()
    dispatch({
      type: types.FETCH_EMAIL_TOPICS,
      payload: topics
    })
  }
}

export function sendEmailTemplate(
  topicId: number,
  templateId: number,
  address: string
): TODOAsyncThunkAction {
  return async dispatch => {
    try {
      EmailTopics.send(topicId, templateId, address)
      dispatch({ type: types.SEND_EMAIL_TEMPLATE_SUCCEEDED })
    } catch (error) {
      logError(JSON.stringify(error))
      dispatch({ type: types.SEND_EMAIL_TEMPLATE_FAILED, payload: error })
    } finally {
      showNotification('Sent!', 'Email template sent!')
    }
  }
}
