import { Api, ApiClient, ApiStatus } from '../client/ApiClient'
import { FromJS } from 'src/js/util/typedMap'
import { List, fromJS } from 'immutable'

interface Email {
  id: number
  email_address: string
  forwarding_addresses: string[]
  status?: string
}
export type ImEmail = FromJS<Email>
export type EmailsListResponse = Email[]
export type ImEmailsListResponse = FromJS<EmailsListResponse>
export interface EmailById {
  [id: number]: Email
  new?: Email
}
export type ImEmailById = FromJS<EmailById>

export interface EmailUpdateResponse extends ApiStatus {
  email: Email
}
export type ImEmailUpdateResponse = FromJS<EmailUpdateResponse>

export class EmailAddressClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async all(): Promise<ImEmailsListResponse> {
    const response = await this.api
      .url('/emails')
      .get()
      .executeOr<ImEmailsListResponse>(List())

    return response
  }
}

export const EmailAddresses = new EmailAddressClient(Api)
