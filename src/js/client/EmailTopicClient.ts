import { Api, ApiClient } from '../client/ApiClient'
import { List, fromJS } from 'immutable'
import { FromJS } from '../util/typedMap'

interface EmailTopic {
  id: number
  name: string
}
export type ImEmailTopic = FromJS<EmailTopic>

type EmailTopicListResponse = EmailTopic[]
export type ImEmailTopicListResponse = FromJS<EmailTopicListResponse>

export interface EmailTopicById {
  [id: number]: EmailTopic
}
export type ImEmailTopicById = FromJS<EmailTopicById>

export class EmailTopicClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async all(): Promise<ImEmailTopicListResponse> {
    const response = await this.api
      .url('/email_topics')
      .get()
      .executeOr<ImEmailTopicListResponse>(List())

    return response
  }

  async send(
    topicId: number,
    templateId: number,
    address: string
  ): Promise<void> {
    const response = await this.api
      .url('/email_topics/send')
      .post({
        topic_id: topicId,
        template_id: templateId,
        sending_address: address
      })
      .execute()

    if (response == null) {
      throw new Error('Got blank response from server when sending email')
    }
  }
}

export const EmailTopics = new EmailTopicClient(Api)
