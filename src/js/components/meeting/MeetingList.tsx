import React, { Component } from 'react'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Button, Col, Row } from 'react-bootstrap'
import { fromJS, Map, Seq } from 'immutable'
import { Link } from 'react-router'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import { Meetings, ImMeeting, Meeting } from '../../client/MeetingClient'
import {
  claimMeetingCode,
  fetchAllMeetings,
  MeetingCodeActions
} from '../../redux/actions/meetingActions'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { FromJS } from 'src/js/util/typedMap'
import { fetchCommittees } from '../../redux/actions/committeeActions'

const meetingActionButtons = fromJS({
  [MeetingCodeActions.AUTOGENERATE]: '♻',
  [MeetingCodeActions.REMOVE]: 'X',
  [MeetingCodeActions.SET]: '💾'
})

const unselected = fromJS<MeetingSelection>({
  meetingId: null,
  code: null,
  codeEdit: null,
  isValid: null
})

interface MeetingSelection {
  meetingId: number | null
  code: number | null
  codeEdit: string | null
  isValid: boolean | null
}
type ImMeetingSelection = FromJS<MeetingSelection>

type MeetingListProps = MeetingListDispatchProps & MeetingListStateProps

interface MeetingListState {
  selected: ImMeetingSelection
}

class MeetingList extends Component<MeetingListProps, MeetingListState> {
  constructor(props) {
    super(props)
    this.state = {
      selected: unselected
    }
  }

  componentWillMount() {
    this.props.fetchAllMeetings()
    this.props.fetchCommittees()
  }

  onFocusMeeting(meetingId: number) {
    if (this.state.selected.get('meetingId') !== meetingId) {
      const code = this.props.meetings.getIn(['byId', meetingId, 'code'])
      const selected: MeetingSelection = {
        meetingId,
        code,
        codeEdit: null,
        isValid: null
      }
      this.setState({
        selected: fromJS(selected)
      })
    }
  }

  onChangeMeetingCode(codeEdit) {
    const meetingId = this.state.selected.get('meetingId')
    const isValid =
      !codeEdit || codeEdit.length === 4
        ? Meetings.isValidMeetingCode(codeEdit)
        : false
    this.setState({
      selected: fromJS({
        meetingId,
        code: this.props.meetings.getIn(['byId', meetingId, 'code']),
        codeEdit,
        isValid
      })
    })
  }

  unselectMeetingCode() {
    this.setState({
      selected: unselected
    })
  }

  submitMeetingCodeAction(meetingId, action) {
    const { codeEdit, isValid } = this.state.selected.toJS()
    const meetingCode =
      '' + (this.props.meetings.getIn(['byId', meetingId, 'code']) || '')
    if (codeEdit && codeEdit !== meetingCode && isValid === false) {
      throw new Error(
        `Cannot set invalid meeting code for meetingId=${meetingId}: ${codeEdit}`
      )
    }
    if (
      action === MeetingCodeActions.SET &&
      (codeEdit === '' || codeEdit == null)
    ) {
      action = MeetingCodeActions.REMOVE
    }
    switch (action) {
      case MeetingCodeActions.AUTOGENERATE:
        // set to valid code if it has changed
        this.props.claimMeetingCode(meetingId, MeetingCodeActions.AUTOGENERATE)
        this.setState({
          selected: this.state.selected.set('codeEdit', null)
        })
        break
      case MeetingCodeActions.SET:
        // claim the given code or generate a random one (if empty)
        if (codeEdit != null) {
          this.props.claimMeetingCode(meetingId, codeEdit)
        } else {
          throw new Error('Error: attempted to claim a null meeting code')
        }
        break
      case MeetingCodeActions.REMOVE:
        // remove meeting code
        this.props.claimMeetingCode(meetingId, '')
        this.unselectMeetingCode()
        break
      default:
        throw new Error(`Unrecognized meeting code action: ${action}`)
    }
  }

  render() {
    const meetings = this.props.meetings
      .get('byId')
      .valueSeq()
      .sortBy((meeting: ImMeeting) => -meeting.get('id', 0)) as Seq.Indexed<
      ImMeeting
    >
    const meetingRows = meetings.map(meeting => {
      const code = '' + (meeting.get('code') || '')
      const codeEdit = this.state.selected.get('codeEdit')
      const meetingId = meeting.get('id')
      const isDirty = codeEdit !== code
      const isSelected = this.state.selected.get('meetingId') === meetingId
      const committeeId = meeting.get('committee_id') || null
      const committeeName = committeeId
        ? `[${this.props.committees.getIn(
            ['byId', committeeId, 'name'],
            'Unknown'
          )} Committee]`
        : ''
      const classNames: string[] = []
      if (isSelected) classNames.push('selected')
      if (isDirty) classNames.push('dirty')
      return (
        <Row key={`meeting_${meetingId}`} className={classNames.join(' ')}>
          <Col sm={3} style={{ marginTop: '4px' }}>
            <span>{meeting.get('name', '[ERROR]') + ' ' + committeeName}</span>
          </Col>
          {this.renderMeeting(meeting)}
        </Row>
      )
    })
    return <div>{meetingRows}</div>
  }

  renderMeeting(meeting) {
    const meetingId = meeting.get('id')
    const code = '' + (meeting.get('code') || '')
    const endTime = meeting.get('end_time')
    const committeeId = meeting.get('committee_id')
    const committee =
      this.props.committees.getIn(['byId', committeeId]) || Map()
    const landingUrl = meeting.get('landing_url')

    const displayProxyLink = endTime && new Date() < new Date(endTime)
    if (
      !isAdmin(this.props.member) ||
      !isCommitteeAdmin(this.props.member, committee.get('name'))
    ) {
      return (
        <Col sm={6}>
          {landingUrl && (
            <span>
              <a href={landingUrl}>Landing Page</a>
              &nbsp;&nbsp;
            </span>
          )}
          {displayProxyLink && (
            <span>
              <Link to={`/meetings/${meetingId}/proxy-token`}>
                Nominate a Proxy
              </Link>
              &nbsp;&nbsp;
            </span>
          )}
          <Link to={`/meetings/${meetingId}/details`}>Details</Link>
        </Col>
      )
    }
    const {
      codeEdit,
      meetingId: selectedMeetingId
    } = this.state.selected.toJS()
    const isSelected = selectedMeetingId === meetingId

    const meetingCodeInput = (
      <input
        type="text"
        key={`meeting_${meetingId}_code`}
        // disabled={isSelected && codeEdit == null}
        value={isSelected && codeEdit != null ? codeEdit : code}
        style={{ width: '6ex', font: '12pt monospace' }}
        maxLength={4}
        onChange={e => {
          this.onChangeMeetingCode(e.target.value)
        }}
        onKeyDown={e => {
          if (e.keyCode === 13) {
            e.preventDefault()
            e.stopPropagation()
            if (this.state.selected.get('isValid')) {
              this.submitMeetingCodeAction(meetingId, MeetingCodeActions.SET)
            }
          }
        }}
        onFocus={e => {
          this.onFocusMeeting(meetingId)
          e.target.select()
        }}
      />
    )

    const actions: string[] = []
    if (code) {
      actions.push(MeetingCodeActions.REMOVE)
    }
    actions.push(MeetingCodeActions.AUTOGENERATE)
    if (isSelected && codeEdit != null && codeEdit !== code) {
      actions.push(MeetingCodeActions.SET)
    }

    const canSave =
      codeEdit !== code &&
      codeEdit != null &&
      Meetings.isValidMeetingCode(codeEdit)
    const actionButtons = actions.map(action => {
      const text = meetingActionButtons.get(action)
      return (
        <button
          disabled={action === MeetingCodeActions.SET && !canSave}
          key={`meeting_${meetingId}_${action}`}
          onClick={() => this.submitMeetingCodeAction(meetingId, action)}
        >
          {text}
        </button>
      )
    })

    return (
      <Col sm={9}>
        {landingUrl && (
          <span>
            <a href={landingUrl}>Landing Page</a>
            &nbsp;&nbsp;
          </span>
        )}
        {displayProxyLink && (
          <span>
            <Link to={`/meetings/${meetingId}/proxy-token`}>
              Nominate a Proxy
            </Link>
            &nbsp;&nbsp;
          </span>
        )}
        <Link to={`/meetings/${meetingId}/details`}>Details</Link>
        &nbsp;&nbsp;
        <Link to={`/meetings/${meetingId}/admin`}>Admin</Link>
        &nbsp;&nbsp;
        <Link to={`/meetings/${meetingId}/kiosk`}>Kiosk</Link>
        &nbsp;&nbsp; Meeting code:{' '}
        <label>
          {code && meetingCodeInput}
          {actionButtons}
        </label>
      </Col>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      claimMeetingCode,
      fetchAllMeetings,
      fetchCommittees
    },
    dispatch
  )

type MeetingListStateProps = RootReducer
type MeetingListDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MeetingListStateProps,
  MeetingListDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MeetingList)
