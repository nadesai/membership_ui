import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Row, Container } from 'react-bootstrap'
import { Map, Seq } from 'immutable'
import dateFormat from 'dateformat'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import EditableAsset, {
  OnSaveHandler,
  ImAssetForm
} from '../asset/EditableAsset'
import { bindActionCreators, Dispatch } from 'redux'
import { searchAssets } from '../../redux/actions/assetActions'
import { fetchCommittees } from '../../redux/actions/committeeActions'
import {
  deleteMeetingAsset,
  fetchAllMeetings,
  saveMeetingAsset
} from '../../redux/actions/meetingActions'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { ImResolvedAsset, ImRawAsset } from 'src/js/client/AssetClient'
import { c } from 'src/js/config'

dateFormat.masks.meetingDetail = 'mmmm d, yyyy h:MM TT'

interface MeetingDetailOwnProps {
  committeeId: number
}
interface MeetingDetailParamProps {
  meetingId: string
}
interface MeetingDetailRouteParamProps {}
type MeetingDetailProps = MeetingDetailOwnProps &
  MeetingDetailStateProps &
  MeetingDetailDispatchProps &
  RouteComponentProps<MeetingDetailParamProps, MeetingDetailRouteParamProps>

class MeetingDetail extends Component<MeetingDetailProps> {
  meetingId: number
  meetingLabels: string[]
  assetPrefix: string

  constructor(props) {
    super(props)
    this.meetingId = parseInt(this.props.params.meetingId)
    this.meetingLabels = [`meeting_id!${this.meetingId}`]
    this.assetPrefix = `meeting/${this.meetingId}/`
  }

  componentWillMount() {
    this.props.fetchCommittees()
    this.props.fetchAllMeetings()
    this.props.searchAssets({ labels: this.meetingLabels })
  }

  createMeetingAsset = async (asset: ImAssetForm, file: File) => {
    const response = await this.props.saveMeetingAsset(asset, file)
    if (response != null) {
      return response
    } else {
      throw new Error("Couldn't create meeting asset")
    }
  }

  updateMeetingAsset = async (asset: ImAssetForm, file: File) => {
    await this.props.saveMeetingAsset(asset, file)
    return asset
  }

  canEditContent = () => {
    const committee: string = this.props.committees.getIn([
      'byId',
      this.props.committeeId,
      'name'
    ])
    const committees = committee ? [committee] : []
    return (
      isAdmin(this.props.member) ||
      isCommitteeAdmin(this.props.member, committees)
    )
  }

  renderExistingAsset = (asset: ImResolvedAsset, onSave?: OnSaveHandler) => {
    const assetId = asset.get('id')
    const assetType = asset.get('content_type')
    return assetId != null ? (
      <EditableAsset
        key={`asset-${assetId}`}
        asset={asset}
        alt={`${assetType} #${assetId} of meeting #${this.meetingId}`}
        prefix={this.assetPrefix}
        labels={this.meetingLabels}
        editable={this.canEditContent()}
        onSave={onSave}
        onDelete={asset => this.props.deleteMeetingAsset(assetId)}
      />
    ) : null
  }

  renderContent() {
    if (this.props.assets.isEmpty()) {
      return null
    }

    const sortedAssets = this.props.assets
      .get('byId')
      .valueSeq()
      .sortBy((a: ImResolvedAsset) => a.get('id')) as Seq.Indexed<
      ImResolvedAsset
    >

    const videos = sortedAssets
      .filter(asset => asset.get('content_type') === 'video')
      .map(asset => this.renderExistingAsset(asset))

    const audio = sortedAssets
      .filter(asset => asset.get('content_type') === 'audio')
      .map(asset => this.renderExistingAsset(asset))

    const images = sortedAssets
      .filter(asset => asset.get('content_type') === 'image')
      .map(asset => this.renderExistingAsset(asset))

    const editContent = (
      <div>
        <PageHeading level={2}>Add Content</PageHeading>
        <EditableAsset
          prefix={this.assetPrefix}
          labels={this.meetingLabels}
          onSave={this.createMeetingAsset}
          onDelete={asset => this.props.deleteMeetingAsset(asset.get('id'))}
        />
      </div>
    )

    const viewContent = (
      <div>
        <PageHeading level={2}>Content</PageHeading>
        <p>
          As part of our meetings, we aim to always inform members if they may
          be photographed or filmed as well as how as that content will be used.
          To that end, please be mindful before sharing content. Always request
          permission from the subject(s) of a photo or video before sharing it
          externally.
        </p>
        <p>
          Check out our{' '}
          <a href={c('URL_CHAPTER_CODE_OF_CONDUCT')}>Code of Conduct</a> for
          more information.
        </p>
        {videos.isEmpty() || <h3>Video</h3>}
        {videos}
        {audio.isEmpty() || <h3>Audio</h3>}
        {audio}
        {images.isEmpty() || <h3>Images</h3>}
        {images}
      </div>
    )

    const emptyContent = (
      <section className="content-list empty-content-list non-ideal-content-list">
        <PageHeading level={2}>Content</PageHeading>
        <PageHeading level={3}>😿</PageHeading>
        <p>
          Nobody has uploaded content for this meeting yet.
          <br />
          Be the change you want to see in the world!
        </p>
      </section>
    )

    return (
      <div>
        {this.canEditContent() && editContent}
        {this.props.assets.get('byId').isEmpty() ? emptyContent : viewContent}
      </div>
    )
  }

  render() {
    const meeting = this.props.meetings.getIn(['byId', this.meetingId]) || Map()
    const meetingName = meeting.get('name')
    const committeeId = meeting.get('committee_id')
    const committeeName = this.props.committees.getIn([
      'byId',
      committeeId,
      'name'
    ])
    const meetingType =
      (committeeName && `${c('GROUP_NAME_SINGULAR')} / ${committeeName}`) ||
      'Chapter Meeting'
    const landingPage = meeting.get('landing_url')
    const startTime = meeting.get('start_time')
    const endTime = meeting.get('end_time')

    return (
      <Container>
        <Row>
          <Col sm={8} lg={6}>
            <div>
              <PageHeading level={1}>{meetingName}</PageHeading>
              <p>
                <b>Meeting Type:</b> {meetingType}
              </p>
              {startTime && (
                <p>
                  <b>Start Time:</b> {dateFormat(startTime, 'meetingDetail')}
                </p>
              )}
              {endTime && (
                <p>
                  <b>End Time:</b> {dateFormat(endTime, 'meetingDetail')}
                </p>
              )}
              {landingPage && (
                <p>
                  <a href={landingPage}>Landing Page</a>
                </p>
              )}
              {this.renderContent()}
            </div>
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      deleteMeetingAsset,
      fetchCommittees,
      fetchAllMeetings,
      saveMeetingAsset,
      searchAssets
    },
    dispatch
  )

type MeetingDetailStateProps = RootReducer
type MeetingDetailDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MeetingDetailStateProps,
  MeetingDetailDispatchProps,
  MeetingDetailOwnProps,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MeetingDetail)
