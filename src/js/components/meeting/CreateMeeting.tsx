import React, { Component } from 'react'
import { connect } from 'react-redux'
import FieldGroup from '../common/FieldGroup'
import { Button, Col, Form, Row, Container } from 'react-bootstrap'
import { List, Map, fromJS } from 'immutable'
import { bindActionCreators, Dispatch } from 'redux'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import * as committees from '../../redux/actions/committeeActions'
import * as meetings from '../../redux/actions/meetingActions'
import PageHeading from '../common/PageHeading'
import { ImMeeting, Meeting } from 'src/js/client/MeetingClient'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import {
  ImCommitteesById,
  ImCommittee,
  CommitteesById
} from 'src/js/client/CommitteeClient'
import { c } from 'src/js/config'
import { capitalize } from 'lodash/fp'
import moment from 'moment'

interface CreateMeetingParamProps {}
interface CreateMeetingRouteParamProps {}

type CreateMeetingOtherProps = RouteComponentProps<
  CreateMeetingParamProps,
  CreateMeetingRouteParamProps
>
type CreateMeetingProps = CreateMeetingStateProps &
  CreateMeetingDispatchProps &
  CreateMeetingOtherProps

interface CreateMeetingState {
  newMeeting: ImMeeting
}

const DEFAULT_GENERAL_COMMITTEE = 'General (no committee)'

class CreateMeeting extends Component<CreateMeetingProps, CreateMeetingState> {
  constructor(props) {
    super(props)
    this.state = {
      newMeeting: fromJS<Meeting>({
        id: 0,
        name: '',
        committee_id: null,
        landing_url: '',
        code: null,
        start_time: moment()
          .minute(0)
          .add(1, 'hour')
          .toDate(),
        end_time: moment()
          .minute(0)
          .add(2, 'hour')
          .toDate()
      })
    }
  }

  componentWillMount() {
    this.props.fetchCommittees()
  }

  canCreateMeeting() {
    return (
      this.state.newMeeting.get('name', '').length > 0 &&
      !this.props.meetings.getIn(['form', 'create', 'inSubmission'], false)
    )
  }

  createMeeting: React.MouseEventHandler<HTMLButtonElement> = () => {
    this.props.createMeeting(this.state.newMeeting).then(() => {
      history.back()
    })
  }

  render() {
    const isGeneralAdmin = isAdmin(this.props.member)
    if (!isGeneralAdmin && !isCommitteeAdmin(this.props.member)) {
      this.props.router.replace('/')
      return null
    }

    const committeeAdminIds = this.props.member
      .getIn(['user', 'data', 'roles'], List())
      .filter(role => role.get('role') === 'admin' && role.get('committee_id'))
      .map(role => role.get('committee_id'))
      .toSet()

    let committeesById = this.props.committees.get(
      'byId',
      fromJS<CommitteesById>({})
    )
    if (!isGeneralAdmin) {
      committeesById = committeesById.filter((committee: ImCommittee) =>
        committeeAdminIds.has(committee.get('id'))
      )
    }
    const committeeIdsByName = committeesById
      .mapEntries<number, string>(([cid, c]: [number, ImCommittee]) => [
        cid,
        c.get('name')
      ])
      .set(0, DEFAULT_GENERAL_COMMITTEE) // add the empty option
      .sort()

    const newMeetingCommitteeId = this.state.newMeeting.get('committee_id')
    const newMeetingCommitteeName =
      newMeetingCommitteeId != null
        ? committeeIdsByName.get(newMeetingCommitteeId)!
        : committeeIdsByName.get(0, DEFAULT_GENERAL_COMMITTEE)

    return (
      <Container className="create-meeting-page">
        <Row>
          <Col sm={8} lg={6}>
            <PageHeading level={2}>Create Meeting</PageHeading>
            <Form onSubmit={e => e.preventDefault()}>
              <FieldGroup
                formKey="name"
                componentClass="input"
                type="text"
                label="Meeting name"
                value={this.state.newMeeting.get('name')}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
                required
              />
              <FieldGroup
                formKey="committee_id"
                componentClass="select"
                options={committeeIdsByName.toObject()}
                label={capitalize(c('GROUP_NAME_SINGULAR'))}
                value={this.mapCommitteeIdToSelectValue(
                  newMeetingCommitteeId
                ).toString()}
                onFormValueChange={(formKey, value) => {
                  this.setState({
                    newMeeting: this.state.newMeeting.set(
                      formKey,
                      this.mapSelectValueToCommitteeId(value)
                    )
                  })
                }}
              />
              <FieldGroup
                formKey="landing_url"
                componentClass="input"
                type="text"
                label="Landing URL"
                helptext="An important link for your meeting. Examples include agenda docs or handout links."
                value={this.state.newMeeting.get('landing_url') || ''}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
              />
              <FieldGroup
                formKey="start_time"
                componentClass="datetime"
                label="Start Time"
                value={this.state.newMeeting.get('start_time')}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
              />
              <FieldGroup
                formKey="end_time"
                componentClass="datetime"
                label="End Time"
                value={this.state.newMeeting.get('end_time')}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
              />
              <button
                type="submit"
                disabled={!this.canCreateMeeting()}
                onClick={this.createMeeting}
              >
                Create Meeting
              </button>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }

  mapCommitteeIdToSelectValue(newMeetingCommitteeId: number | null): number {
    if (newMeetingCommitteeId == null) {
      return 0
    } else {
      return newMeetingCommitteeId
    }
  }

  mapSelectValueToCommitteeId(selectValue: string): number | null {
    const parsed = parseInt(selectValue)
    if (parsed <= 0) {
      return null
    } else {
      return parsed
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      createMeeting: meetings.createMeeting,
      fetchCommittees: committees.fetchCommittees
    },
    dispatch
  )

type CreateMeetingStateProps = RootReducer
type CreateMeetingDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CreateMeetingStateProps,
  CreateMeetingDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(CreateMeeting)
