import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container } from 'react-bootstrap'
import { Link } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import dateFormat from 'dateformat'
import { isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'
import { fetchAllMeetings } from '../../redux/actions/meetingActions'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { c } from 'src/js/config'
import { ImMeeting } from 'src/js/client/MeetingClient'

dateFormat.masks.meetingDetail = 'mmmm d, yyyy h:MM TT'

type MyMeetingsProps = MyMeetingsDispatchProps & MyMeetingsStateProps

class MyMeetings extends Component<MyMeetingsProps> {
  componentDidMount() {
    this.props.fetchAllMeetings()
  }

  componentDidUpdate(prevProps: MyMeetingsProps) {
    const prevUser = prevProps.member.getIn(['user', 'data'])
    const currentUser = this.props.member.getIn(['user', 'data'])

    if (prevUser !== currentUser) {
      this.props.fetchAllMeetings()
    }
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    return (
      <Container>
        <PageHeading level={1}>My Meetings</PageHeading>
        <h3>What meetings have I attended?</h3>
        {this.renderAttendance()}
        <h3>What meetings are upcoming?</h3>
        {this.renderUpcoming()}
        <h3>Where can I find the rest of the meetings?</h3>
        <p>
          Check out our <Link to={`/meetings`}>full list of meetings</Link>.
        </p>
      </Container>
    )
  }

  renderAttendance() {
    const memberData = this.props.member.getIn(['user', 'data'])

    const meetings = memberData
      .get('meetings')
      .reverse()
      .map(meeting => {
        const meetingId = meeting.get('meeting_id')
        const meetingName = meeting.get('name')

        return (
          <div key={`meeting-${meetingId}`}>
            <a href={`meetings/${meetingId}/details`}>{meetingName}</a>
          </div>
        )
      })

    if (meetings.size === 0) {
      return (
        <p>
          You haven't attended any chapter meetings yet. To find our upcoming
          events, check out{' '}
          <a href={c('URL_CHAPTER_EVENTS')}>our events page</a>.
        </p>
      )
    } else {
      return (
        <div>
          <p>You've attended the following meetings so far:</p>
          {meetings}
        </div>
      )
    }
  }

  renderUpcoming() {
    const memberData = this.props.member.getIn(['user', 'data'])
    const memberRoles = memberData.get('roles')

    const upcomingMeetings = this.props.meetings
      .get('byId')
      .filter((meeting: ImMeeting) => {
        if (!meeting) return false
        // Only show meetings in the future
        const startTime = meeting.get('start_time')
        if (!startTime || startTime.getTime() < Date.now()) {
          return false
        }
        const committeeId = meeting.get('committee_id')
        // If committee_id is null, this is a general meeting; show it
        if (committeeId === null) {
          return true
        }
        // Otherwise, only show the meeting if it's for a committee that the
        // user is a memebr of
        if (memberRoles.find(role => role.committee_id === committeeId)) {
          return true
        }
        return false
      })

    if (upcomingMeetings.size === 0) {
      return <p>There are no upcoming meetings.</p>
    }

    return upcomingMeetings.toList().map((meeting: ImMeeting) => {
      const meetingId = meeting.get('id')
      const meetingName = meeting.get('name')
      const meetingStartTime = meeting.get('start_time')

      return (
        <div key={`meeting-${meetingId}`}>
          <a href={`meetings/${meetingId}/details`}>{meetingName}</a> -{' '}
          {dateFormat(meetingStartTime, 'meetingDetail')}
        </div>
      )
    })
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchAllMeetings }, dispatch)

type MyMeetingsStateProps = RootReducer
type MyMeetingsDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MyMeetingsStateProps,
  MyMeetingsDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MyMeetings)
