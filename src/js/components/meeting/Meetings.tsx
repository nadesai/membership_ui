import React, { Component } from 'react'
import { Container } from 'react-bootstrap'
import { Link } from 'react-router'
import MeetingList from './MeetingList'
import PageHeading from '../common/PageHeading'
import { isCommitteeAdmin, isAdmin } from 'src/js/services/members'
import { connect } from 'react-redux'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ImMemberState } from 'src/js/redux/reducers/memberReducers'

interface MeetingsPageStateProps {
  member: ImMemberState
}
type MeetingsPageProps = MeetingsPageStateProps

class Meetings extends Component<MeetingsPageProps> {
  render() {
    const hasPrivileges =
      isAdmin(this.props.member) || isCommitteeAdmin(this.props.member)

    return (
      <Container>
        <PageHeading level={2}>Meetings</PageHeading>
        {hasPrivileges && (
          <div style={{ marginBottom: '2rem' }}>
            <Link to={`/meetings/create`}>
              <button>Create Meeting</button>
            </Link>
          </div>
        )}
        <MeetingList />
      </Container>
    )
  }
}

const mapStateToProps = (state: RootReducer): MeetingsPageStateProps => ({
  member: state.member
})

export default connect<MeetingsPageStateProps, {}, null, RootReducer>(
  mapStateToProps
)(Meetings)
