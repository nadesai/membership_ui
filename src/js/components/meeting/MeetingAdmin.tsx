import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Col,
  Form,
  FormControl,
  Row,
  Container,
  TabContainer,
  Tabs,
  Tab,
  Button
} from 'react-bootstrap'
import { Map, fromJS } from 'immutable'
import DateTime from 'react-datetime'
import {
  Meetings,
  ImMeeting,
  UpdateAttendeeAttributes,
  ImPartialMeeting
} from '../../client/MeetingClient'
import MemberSearchField from '../common/MemberSearchField'
import { AttendeeEligibilityRow, EmptyEligibilityRow } from '../admin/MemberRow'
import VoteEligiblePopup from './VoteEligiblePopup'
import {
  EligibleMemberListEntry,
  UpdateMemberAttributes,
  ImEligibleMemberList,
  ImEligibleMemberListEntry
} from 'src/js/client/MemberClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import Loading from 'src/js/components/common/Loading'
import moment, { Moment } from 'moment'

type MeetingAdminStateProps = RootReducer
interface MeetingAdminParamProps {
  meetingId: string
}
interface MeetingAdminRouteParamProps {}
type MeetingAdminProps = MeetingAdminStateProps &
  RouteComponentProps<MeetingAdminParamProps, MeetingAdminRouteParamProps>

interface MeetingAdminState {
  meeting: ImMeeting | null
  attendees: ImEligibleMemberList | null
  inSubmission: boolean
  lastAddedAttendee: ImEligibleMemberListEntry | null
  tabKey: string
}

class MeetingAdmin extends Component<MeetingAdminProps, MeetingAdminState> {
  constructor(props) {
    super(props)
    this.state = {
      meeting: null,
      attendees: null,
      inSubmission: false,
      lastAddedAttendee: null,
      tabKey: 'details'
    }
  }

  componentDidMount() {
    this.getMeeting()
    this.getMeetingAttendees()
  }

  render() {
    const attendees = this.state.attendees

    if (this.state.meeting == null) {
      return <Loading />
    }

    return (
      <Container className="meeting-admin">
        <h2>
          {this.state.meeting.get('name')}{' '}
          <small>
            {this.state.meeting.get('code')
              ? '#' + this.state.meeting.get('code')
              : ''}
          </small>
        </h2>

        <Row>
          <Col>
            <Tabs
              id="meeting-tabs"
              activeKey={this.state.tabKey}
              onSelect={this.handleChangeTab}
              className="meeting-tabs"
            >
              <Tab eventKey="details" title="Meeting details">
                <h3>Edit Meeting</h3>
                <Form
                  className="edit-meeting-form"
                  onSubmit={e => e.updateLandingUrl()}
                >
                  <Form.Group as={Row} controlId="formLandingUrl">
                    <Form.Label column sm={2}>
                      Landing URL
                    </Form.Label>
                    <Col sm={6}>
                      <FormControl
                        type="text"
                        value={
                          this.state.meeting.get('landing_url') || undefined
                        }
                        onChange={e => this.updateLandingUrl(e.target.value)}
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm={2}>
                      Start Time
                    </Form.Label>
                    <Col sm={6}>
                      <DateTime
                        value={
                          this.state.meeting.get('start_time') || undefined
                        }
                        onChange={m => this.updateStartTime(m)}
                        timeConstraints={{
                          minutes: { min: 0, max: 59, step: 15 }
                        }}
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group column as={Row}>
                    <Form.Label column sm={2}>
                      End Time
                    </Form.Label>
                    <Col sm={6}>
                      <DateTime
                        value={this.state.meeting.get('end_time') || undefined}
                        onChange={m => this.updateEndTime(m)}
                        timeConstraints={{
                          minutes: { min: 0, max: 59, step: 15 }
                        }}
                      />
                    </Col>
                  </Form.Group>
                  <button type="submit" onClick={e => this.submitMeeting(e)}>
                    Save changes
                  </button>
                </Form>
              </Tab>
              <Tab eventKey="signin" title="Assisted sign-in">
                <h3>Sign In</h3>
                <div className="meeting-member-search-container">
                  <MemberSearchField
                    onMemberSelected={e => this.onMemberSelected(e)}
                  />
                  <VoteEligiblePopup
                    numVotes={
                      this.state.lastAddedAttendee
                        ? this.state.lastAddedAttendee
                            .get('eligibility')
                            .get('num_votes')
                        : 0
                    }
                    visible={this.state.lastAddedAttendee != null}
                  />
                </div>
                <AttendeesList
                  attendees={attendees}
                  meeting={this.state.meeting}
                />
              </Tab>
            </Tabs>
          </Col>
        </Row>
      </Container>
    )
  }

  handleChangeTab = (tabKey: string) => {
    this.setState({ tabKey })
  }

  updateLandingUrl(landingUrl) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: this.state.meeting.set('landing_url', landingUrl)
    })
  }

  updateStartTime(startTime: string | Moment) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    if (typeof startTime === 'string') {
      this.setState({
        meeting: this.state.meeting.set(
          'start_time',
          moment(startTime).toDate()
        )
      })
    } else {
      this.setState({
        meeting: this.state.meeting.set('start_time', startTime.toDate())
      })
    }
  }

  updateEndTime(endTime: string | Moment) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    if (typeof endTime === 'string') {
      this.setState({
        meeting: this.state.meeting.set('end_time', moment(endTime).toDate())
      })
    } else {
      this.setState({
        meeting: this.state.meeting.set('end_time', endTime.toDate())
      })
    }
  }

  async submitMeeting(e) {
    e.preventDefault()

    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }
    this.setState({
      inSubmission: true
    })

    try {
      const attributeNames = ['landing_url', 'start_time', 'end_time']
      const updatedAttributes = this.state.meeting
        // filter out attribute names we don't want to send up
        .filter((_, key) => attributeNames.includes(key))
        // treat falsy values as null when sending up
        .map((val, _) => val || null) as ImPartialMeeting
      // send up ISO 8601 strings
      if (updatedAttributes.includes('start_time')) {
        updatedAttributes['start_time'] = updatedAttributes[
          'start_time'
        ].toISOString()
      }
      if (updatedAttributes.includes('end_time')) {
        updatedAttributes['end_time'] = updatedAttributes[
          'end_time'
        ].toISOString()
      }

      const meeting = await Meetings.updateMeeting(
        this.state.meeting.get('id'),
        updatedAttributes
      )
      const updatedMeeting = this.state.meeting.merge(meeting) as ImMeeting
      this.setState({ meeting: updatedMeeting })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  async onMemberSelected(member: EligibleMemberListEntry) {
    await Meetings.addAttendee(parseInt(this.props.params.meetingId), member.id)
    this.setState({ lastAddedAttendee: fromJS(member) })
    setTimeout(() => {
      this.setState({ lastAddedAttendee: null })
    }, 5000)
    this.getMeetingAttendees()
  }

  async getMeetingAttendees() {
    await this.getMeeting()

    const meetingId = parseInt(this.props.params.meetingId)
    const results = await Meetings.getAttendees(meetingId)
    this.setState({ attendees: results.reverse() })
  }

  async getMeeting() {
    const meeting = await Meetings.get(parseInt(this.props.params.meetingId))

    if (meeting != null) {
      this.setState({ meeting: meeting })
    }
  }
}

const updateAttendeeClicked = async (
  member: ImEligibleMemberListEntry,
  meeting: ImMeeting,
  attributes: UpdateAttendeeAttributes
) => {
  await Meetings.updateAttendee(meeting.get('id'), member.get('id'), attributes)
  this.getMeetingAttendees()
}

const removeAttendeeClicked = async (
  member: ImEligibleMemberListEntry,
  meeting: ImMeeting
) => {
  if (meeting != null) {
    const meetingName = meeting.get('name')
    if (
      confirm(
        `Are you sure you want to remove ${member.get(
          'name'
        )} from the list of attendees for ${meetingName}?`
      )
    ) {
      await Meetings.removeAttendee(meeting.get('id'), member.get('id'))
      this.getMeetingAttendees()
    }
  }
}

const countEligibleAttendees = (attendees: ImEligibleMemberList): number => {
  return attendees.filter(
    attendee =>
      attendee.hasIn(['eligibility', 'is_eligible']) &&
      attendee.getIn(['eligibility', 'is_eligible'])
  ).size
}

const countProxyVotes = (attendees: ImEligibleMemberList): number => {
  return attendees.reduce((result, attendee) => {
    const isPersonallyEligible =
      attendee.hasIn(['eligibility', 'is_eligible']) &&
      attendee.getIn(['eligibility', 'is_eligible'])
    const numVotes = attendee.getIn(
      ['eligibility', 'num_votes'],
      // if num_votes was not passed, assume is_eligible counts as 1
      isPersonallyEligible ? 1 : 0
    )
    const expectedNumVotes = isPersonallyEligible ? 1 : 0
    const numProxyVotes = numVotes - expectedNumVotes
    return result + numProxyVotes
  }, 0)
}

const isGeneralMeeting = (meeting: ImMeeting | null): boolean => {
  if (meeting != null) {
    return (
      Boolean(meeting.get('name').match(/General/)) &&
      meeting.get('committee_id') == null
    )
  } else {
    return false
  }
}

interface AttendeeEntryProps {
  attendee: ImEligibleMemberListEntry
  meeting: ImMeeting
}

const AttendeeEntry: React.FC<AttendeeEntryProps> = ({ attendee, meeting }) => (
  <div className="attendee-entry">
    <span>{`${attendee.get('name')}`}</span>
    <Button
      className="attendee-remove"
      onClick={e => removeAttendeeClicked(attendee, meeting)}
      variant="danger"
    >
      remove
    </Button>
    {meeting != null && (
      <Button
        className="attendee-update-eligibility-to-vote"
        onClick={e =>
          updateAttendeeClicked(attendee, meeting, {
            update_eligibility_to_vote: true
          })
        }
        variant="outline-info"
      >
        mark as eligible to vote
      </Button>
    )}
    {isGeneralMeeting(meeting) ? (
      attendee.hasIn(['eligibility']) ? (
        <AttendeeEligibilityRow
          isPersonallyEligible={attendee.getIn(['eligibility', 'is_eligible'])}
          numVotes={attendee.getIn(
            ['eligibility', 'num_votes'],
            // if num_votes was not passed, assume is_eligible counts as 1
            attendee.getIn(['eligibility', 'is_eligible']) ? 1 : 0
          )}
        />
      ) : (
        <EmptyEligibilityRow />
      )
    ) : null}
  </div>
)

interface AttendeesListProps {
  attendees: ImEligibleMemberList | null
  meeting: ImMeeting | null
}

const AttendeesList: React.FC<AttendeesListProps> = ({
  attendees,
  meeting
}) => {
  if (attendees == null || meeting == null || attendees.size === 0) {
    return null
  }

  return (
    <section className="attendees-list">
      <h3>{'Attendees ' + (attendees ? '(' + attendees.size + ')' : '')}</h3>
      {isGeneralMeeting(meeting) && attendees != null ? (
        <small>
          <i>Eligible voters: {countEligibleAttendees(attendees)}</i>
          <br />
          <i>
            Number of proxy votes: {countProxyVotes(attendees)} (already
            included in Eligible voter count)
          </i>
        </small>
      ) : null}
      {attendees.map(attendee => {
        return <AttendeeEntry attendee={attendee} meeting={meeting} />
      })}
    </section>
  )
}

export default connect<MeetingAdminStateProps, null, {}, RootReducer>(
  state => state
)(MeetingAdmin)
