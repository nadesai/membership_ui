import dateFormat from 'dateformat'
import { fromJS } from 'immutable'
import { capitalize } from 'lodash/fp'
import React, { Component } from 'react'
import {
  Button,
  Col,
  Container,
  Form,
  Modal,
  Row,
  Tab,
  Tabs
} from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { FiTrash } from 'react-icons/fi'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import EditMemberModal from 'src/js/components/membership/EditMemberModal'
import MemberNotesTextarea from 'src/js/components/membership/MemberNotesTextarea'
import { c, PAGE_TITLE_TEMPLATE, USE_ELECTIONS } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import {
  ImMemberDetailed,
  ImMemberRole,
  ImMembership,
  Members
} from '../../client/MemberClient'
import { fetchElections } from '../../redux/actions/electionActions'
import { membershipApi } from '../../services/membership'
import { HTTP_POST, logError } from '../../util/util'
import AddEligibleVoter from '../admin/AddEligibleVoter'
import AddMeeting from '../admin/AddMeeting'
import AddRole from '../admin/AddRole'
import Loading from '../common/Loading'
import PageHeading from '../common/PageHeading'
import EligibleVotes from './EligibleVotes'

dateFormat.masks.dsaElection = "dddd, mmmm dS 'at' h:MM TT"

interface MemberPageParamProps {
  memberId: string
}
interface MemberPageRouteParamProps {}
type MemberPageProps = MemberPageStateProps &
  MemberPageDispatchProps &
  RouteComponentProps<MemberPageParamProps, MemberPageRouteParamProps>

type MemberPageTabKey = 'committees' | 'meetings' | 'elections' | 'notes'

interface MemberPageState {
  member: ImMemberDetailed | null
  inSubmission: boolean
  meetingShortId: string
  tabKey: MemberPageTabKey
  showRemoveRolePrompt: string | null
  showEditInfoModal: boolean
}

class MemberPage extends Component<MemberPageProps, MemberPageState> {
  constructor(props) {
    super(props)
    this.state = {
      member: null,
      inSubmission: false,
      meetingShortId: '',
      tabKey: 'committees',
      showRemoveRolePrompt: null,
      showEditInfoModal: false
    }
  }

  componentDidMount() {
    this.fetchMemberData()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.fetchMemberData()
    }
  }

  setKey = (k: MemberPageTabKey) => {
    this.setState({ tabKey: k })
  }

  showRemoveRolePrompt = (roleKey: string) => {
    this.setState({ showRemoveRolePrompt: roleKey })
  }

  hideRemoveRolePrompt = () => {
    this.setState({ showRemoveRolePrompt: null })
  }

  showEditInfoModal = () => {
    this.setState({ showEditInfoModal: true })
  }

  hideEditInfoModal = () => {
    this.setState({ showEditInfoModal: false })
  }

  handleSaveComplete = () => {
    this.fetchMemberData()
  }

  private getNameFromMember(memberData: ImMemberDetailed) {
    return `${memberData.getIn(['info', 'first_name'])} ${memberData.getIn([
      'info',
      'last_name'
    ])}`
  }

  render() {
    if (this.state.member === null) {
      return <Loading />
    }

    return (
      <Container className="admin-page member-page">
        <Row>
          <Col md={12}>
            <PageHeading level={1}>Manage member info</PageHeading>
          </Col>
        </Row>
        <Row>
          <Col md={4}>{this.renderMemberInfo()}</Col>
          <Col md={8}>
            <Tabs
              id="member-page-tabs"
              activeKey={this.state.tabKey}
              onSelect={k => this.setKey(k)}
            >
              <Tab
                eventKey="committees"
                title={`${capitalize(c('GROUP_NAME_PLURAL'))} and roles`}
              >
                {this.renderCommittees()}
              </Tab>
              <Tab eventKey="meetings" title="Meetings">
                {this.renderMeetings()}
              </Tab>
              <Tab eventKey="notes" title="Notes">
                {this.renderNotes()}
              </Tab>
              {USE_ELECTIONS && (
                <Tab eventKey="elections" title="Elections">
                  {USE_ELECTIONS && this.renderEligibleVoters()}
                </Tab>
              )}
            </Tabs>
          </Col>
        </Row>
      </Container>
    )
  }

  renderMemberInfo() {
    const memberData = this.state.member

    if (memberData != null) {
      const doNotEmail = memberData.get('do_not_email')
      const doNotCall = memberData.get('do_not_call')
      const name = this.getNameFromMember(memberData)
      const membership = memberData.get('membership') as
        | ImMembership
        | undefined
      const info = memberData.get('info')

      return (
        <section className="member-info-section">
          <Helmet>
            <title>{name}</title>
          </Helmet>
          <PageHeading level={2}>{name}</PageHeading>
          <dl className="member-info">
            <div className="info-email-address">
              <dt>Email address</dt>
              <dd>{info.get('email_address')}</dd>
            </div>
            <div className="info-membership">
              <dt>DSA membership</dt>
              <dd>
                <MembershipStatus membership={membership ?? null} />
              </dd>
            </div>
            <div className="info-email-optout info-comms-optout">
              <dt>Okay to email?</dt>
              <dd>
                {doNotEmail ? (
                  <span className="comms-opt-out">No, do not email</span>
                ) : (
                  <span className="comms-okay">Yes, email okay</span>
                )}
              </dd>
            </div>
            <div className="info-phone-optout info-comms-optout">
              <dt>Okay to call?</dt>
              <dd>
                {doNotCall ? (
                  <span className="comms-opt-out">No, do not call</span>
                ) : (
                  <span className="comms-okay">Yes, calls okay</span>
                )}
              </dd>
            </div>
            {info.get('biography') != null && (
              <div className="info-biography">
                <dt>Biography</dt>
                <dd>{info.get('biography')}</dd>
              </div>
            )}
          </dl>
          <EditMemberModal
            show={this.state.showEditInfoModal}
            onHide={this.hideEditInfoModal}
            member={memberData}
            onSaveComplete={this.handleSaveComplete}
          />
          <Button onClick={this.showEditInfoModal} variant="primary">
            Edit info...
          </Button>
        </section>
      )
    } else {
      return null
    }
  }

  renderCommittees() {
    if (this.state.member == null) {
      return null
    }

    const memberId = this.state.member.get('id')
    const memberName = this.getNameFromMember(this.state.member)
    const roles = this.state.member
      .get('roles')
      .sortBy(role => role.get('committee'))
      .map((role: ImMemberRole, index) => {
        const roleName = role.get('role')
        const roleCommittee = role.get('committee')
        const roleKey = `role-${index}-${roleName}-${roleCommittee}`
        return (
          <div key={roleKey} className="committee-member">
            <button
              onClick={e => this.showRemoveRolePrompt(roleKey)}
              aria-label={`Remove role ${roleName} from member`}
            >
              <FiTrash />
            </button>
            {`${roleCommittee}: ${roleName}`}
            <Modal
              show={this.state.showRemoveRolePrompt === roleKey}
              onHide={this.hideRemoveRolePrompt}
            >
              <Modal.Header closeButton>
                <Modal.Title>Confirm role removal</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                Are you sure you want to remove the{' '}
                <strong>
                  {roleCommittee} {roleName}
                </strong>{' '}
                role from the member{' '}
                <strong>{this.getNameFromMember(this.state.member!)}</strong>?
              </Modal.Body>
              <Modal.Footer>
                <Button
                  onClick={e => this.removeRole(memberId, role)}
                  variant="danger"
                >
                  Remove role
                </Button>
                <Button
                  onClick={this.hideRemoveRolePrompt}
                  variant="outline-secondary"
                >
                  Cancel
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        )
      })

    return (
      <>
        <section className="member-page-section committees-section">
          <PageHeading level={3}>
            Current {c('GROUP_NAME_PLURAL')} and roles
          </PageHeading>
          {roles.size > 0 ? (
            roles
          ) : (
            <span className="empty-committees list">
              This member is not part of any {c('GROUP_NAME_PLURAL')}.
            </span>
          )}
        </section>
        <AddRole
          memberId={
            this.props.params.memberId != null
              ? parseInt(this.props.params.memberId)
              : this.state.member.get('id')
          }
          refresh={() => this.fetchMemberData()}
        />
      </>
    )
  }

  renderMeetings() {
    if (this.state.member != null) {
      const meetings = this.state.member
        .get('meetings')
        .map((meeting, index) => (
          <div key={`meeting-${index}`}>{meeting.get('name')}</div>
        ))

      return (
        <>
          <section className="meetings-attended-section member-page-section">
            <PageHeading level={3}>Meetings attended</PageHeading>
            {meetings}
          </section>
          <AddMeeting
            memberId={
              this.props.params.memberId != null
                ? parseInt(this.props.params.memberId)
                : this.state.member.get('id')
            }
            refresh={() => this.fetchMemberData()}
          />
          <section className="meeting-code-section member-page-section">
            <PageHeading level={3}>
              Submit attendance using a meeting code
            </PageHeading>
            <Form
              inline
              onSubmit={e => {
                this.attendMeeting(e)
              }}
            >
              <label htmlFor="meetingCode">Meeting Code: #</label>{' '}
              <input
                id="meetingCode"
                type="text"
                placeholder="0000"
                maxLength={4}
                value={this.state.meetingShortId}
                onChange={e => {
                  const meetingCode = e.target.value
                  this.setState({ meetingShortId: meetingCode })
                }}
              />
              <button type="submit">Attend</button>
            </Form>
          </section>
        </>
      )
    } else {
      return null
    }
  }

  renderNotes() {
    const memberData = this.state.member
    if (memberData == null) {
      return null
    }

    const notes = memberData.get('notes')

    return (
      <section className="member-notes-section member-page-section">
        <PageHeading level={3}>Member notes</PageHeading>
        <p className="member-notes-disclaimer">
          Any notes you type here are visible to other officers and co-chairs in
          the chapter, but are currently <strong>not visible</strong> to this
          member or other members.
        </p>
        <MemberNotesTextarea memberId={memberData.get('id')} notes={notes} />
      </section>
    )
  }

  renderEligibleVoters() {
    if (this.state.member != null) {
      return (
        <div>
          <EligibleVotes
            votes={this.state.member.get('votes')}
            elections={this.props.elections.get('byId')}
          />
          <AddEligibleVoter
            memberId={
              this.props.params.memberId != null
                ? parseInt(this.props.params.memberId)
                : this.state.member.get('id')
            }
            refresh={() => this.fetchMemberData()}
          />
        </div>
      )
    } else {
      return null
    }
  }

  async fetchMemberData() {
    try {
      this.setState({
        member: await Members.details(this.props.params.memberId)
      })
    } catch (err) {
      return logError('Error loading member details', err)
    }

    if (USE_ELECTIONS) {
      this.props.fetchElections()
    }
  }

  async removeRole(memberId: number, role: ImMemberRole) {
    const committeeId =
      role.get('committee_id') === -1 ? null : role.get('committee_id')

    await Members.removeRole(memberId, role.get('role'), committeeId)
    this.fetchMemberData()
  }

  async attendMeeting(e) {
    e.preventDefault()
    if (this.state.inSubmission || this.state.member == null) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const meeting = await membershipApi(HTTP_POST, '/meeting/attend', {
        meeting_short_id: this.state.meetingShortId,
        member_id: this.props.params.memberId
          ? this.props.params.memberId
          : this.state.member.get('id')
      })
      this.setState({ meetingShortId: '' })
      const landingUrl = fromJS(meeting).get('landing_url', null)
      if (landingUrl) {
        // TODO: Figure out how to inject this for testing
        location.href = landingUrl
      }
      this.fetchMemberData()
    } catch (err) {
      return logError('Error adding attendee', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

const isMembershipExpired = (
  membership: ImMembership,
  now: Date = new Date()
) => (membership.get('dues_paid_until') as Date) < now

interface MembershipStatusProps {
  membership: ImMembership | null
}

const MembershipStatus: React.FC<MembershipStatusProps> = ({ membership }) => {
  if (membership == null) {
    return <span className="no-membership">No membership found</span>
  }

  const duesDate = membership.get('dues_paid_until') as Date

  if (isMembershipExpired(membership)) {
    return (
      <span className="membership-expired">
        Membership expired on {duesDate.toLocaleDateString()}
      </span>
    )
  }

  return (
    <span className="membership-valid">
      Membership valid through {duesDate.toLocaleDateString()}
    </span>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchElections }, dispatch)

type MemberPageStateProps = RootReducer
type MemberPageDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MemberPageStateProps,
  MemberPageDispatchProps,
  null,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MemberPage)
