import React, { useState } from 'react'
import { Alert, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { FiCheckCircle, FiSave, FiXCircle } from 'react-icons/fi'
import { useMutation } from 'react-query'
import AutoTextarea from 'react-textarea-autosize'
import { useDebounce } from 'react-use'
import { Members } from 'src/js/client/MemberClient'

interface MembershipNotesTextareaProps {
  memberId: number
  notes: string
  onChange?: React.ChangeEventHandler<HTMLTextAreaElement>
}

const updateNotes = ({ memberId, notes }) =>
  Members.updateNotes(memberId, notes)

const MemberNotesTextarea: React.FC<MembershipNotesTextareaProps> = ({
  memberId,
  notes,
  onChange
}) => {
  const [contents, setContents] = useState(notes)
  const [mutate, { data, isLoading, error }] = useMutation(updateNotes)

  const saveChanges = async () => {
    if (contents != notes) {
      const data = await mutate({ memberId, notes: contents })

      if (data != null) {
        setContents(data.get('notes'))
      }
    }
  }

  const [, cancel] = useDebounce(saveChanges, 2000, [contents])

  const handleSaveChanges: React.MouseEventHandler<HTMLButtonElement> = e => {
    saveChanges()
  }

  const handleTextChange: React.ChangeEventHandler<HTMLTextAreaElement> = e => {
    cancel()
    setContents(e.target.value)

    if (onChange != null) {
      onChange(e)
    }
  }

  return (
    <div className="member-notes-textarea-container">
      <AutoTextarea
        className="notes-textarea"
        value={contents}
        onChange={handleTextChange}
        minRows={10}
        maxRows={20}
      />
      <footer className="notes-footer">
        <div className="notes-state">
          {!isLoading && data == null && contents != null && (
            <Alert className="textarea-original" variant="info">
              <FiSave /> No edits since last save.
            </Alert>
          )}
          {isLoading && (
            <Alert className="textarea-loading" variant="info">
              <FiSave /> Saving notes...
            </Alert>
          )}
          {!isLoading && error == null && data != null && (
            <Alert className="textarea-saved" variant="success">
              <FiCheckCircle /> Notes successfully saved.
            </Alert>
          )}
          {error != null && (
            <Alert className="textarea-error" variant="danger">
              <FiXCircle /> Issue when saving notes! Your changes may not have
              been saved. Copy your notes for safe-keeping.
            </Alert>
          )}
        </div>
        <OverlayTrigger
          placement="right"
          overlay={
            <Tooltip id="tooltip-save">
              We save automatically for you as you type but it never hurts to be
              sure 😉
            </Tooltip>
          }
        >
          <Button variant="success" onClick={handleSaveChanges}>
            Save changes
          </Button>
        </OverlayTrigger>
      </footer>
    </div>
  )
}

export default MemberNotesTextarea
