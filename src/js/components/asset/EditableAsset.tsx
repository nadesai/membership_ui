import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fromJS, List, Map } from 'immutable'
import { PRESIGNED_URL, EXTERNAL_URL } from '../../models/assets'
import {
  AssetClient,
  ResolvedAssetContentType,
  RawAsset,
  ImResolvedAsset,
  ResolvedAsset,
  AssetsById
} from '../../client/AssetClient'
import { deleteAsset } from '../../redux/actions/assetActions'
import {
  contentTypeForFile,
  contentTypeForUrl,
  sanitizeFileName
} from '../../util/assetUtils'
import { typedMerge, FromJS, ImmutableKeyValue } from '../../util/typedMap'
import { Modify } from '../../util/typeUtils'
import { RootReducer } from '../../redux/reducers/rootReducer'
import { ToggleButtonGroup, ToggleButton } from 'react-bootstrap'
import { isFunction } from 'lodash/fp'

type EmptyResolvedAsset = Modify<ResolvedAsset, { id: number | null }>
type ImEmptyResolvedAsset = FromJS<EmptyResolvedAsset>
const EMPTY_ASSET: ImEmptyResolvedAsset = fromJS<EmptyResolvedAsset>({
  id: null,
  title: '',
  asset_type: null,
  content_type: '',
  labels: [],
  relative_path: '',
  last_updated: '',
  view_url: ''
})

type AssetForm = Modify<
  RawAsset,
  {
    id: number | null
    content_type: ResolvedAssetContentType | null
    last_updated: string | null
  }
>
export type ImAssetForm = FromJS<AssetForm>

const EMPTY_FORM: ImAssetForm = fromJS<AssetForm>({
  id: null,
  title: '',
  last_updated: null,
  asset_type: null,
  content_type: null,
  labels: [],
  relative_path: ''
})

const RESET_STATE: Partial<EditableAssetState> = {
  asset: EMPTY_ASSET,
  form: EMPTY_FORM,
  existingAssetUrl: '',
  newAssetPath: '',
  newAssetFile: null,
  newTitle: '',
  publicAssetUrl: ''
}

type ModeType =
  | 'attach_public_asset'
  | 'attach_protected_asset'
  | 'upload_protected_asset'

const Mode = Object.freeze<{ [key: string]: ModeType }>({
  ATTACH_PUBLIC_ASSET: 'attach_public_asset',
  ATTACH_PROTECTED_ASSET: 'attach_protected_asset',
  UPLOAD_PROTECTED_ASSET: 'upload_protected_asset'
})

interface EditableAssetOwnProps {
  asset?: ImResolvedAsset
  prefix: string
  labels: string[]
  onSave?: OnSaveHandler
  onDelete?: OnDeleteHandler
  onSaveExistingProtectedAsset?: OnSaveHandler
  onSavePublicAsset?: OnSaveHandler
  onUploadAndSaveProtectedAsset?: OnSaveHandler
  defaultMode?: ModeType
  alt?: string
  calcDefaultPath?(f: File): string
  afterSave?: (form: ImAssetForm, file?: File) => void
  afterDelete?: () => void
  src?: string
  editable?: boolean
  form?: ImAssetForm
}

type EditableAssetStateProps = RootReducer

type EditableAssetProps = EditableAssetOwnProps & EditableAssetStateProps

export type OnSaveHandler = (
  form: ImAssetForm,
  file?: File
) => Promise<ImResolvedAsset>
export type OnDeleteHandler = (asset: ImResolvedAsset) => void
interface EditableAssetState {
  asset: ImEmptyResolvedAsset
  assetClient?: AssetClient
  editing: boolean
  editable: boolean
  emptyForm: ImAssetForm
  emptyState: Partial<EditableAssetState>
  form: ImAssetForm
  labels: string[]
  mode: ModeType | null
  prefix: string
  afterSave?: (form: ImAssetForm, file?: File) => void
  afterDelete?: () => void
  onDelete?: OnDeleteHandler
  onSave?: OnSaveHandler
  onSaveExistingProtectedAsset?: OnSaveHandler
  onSavePublicAsset?: OnSaveHandler
  onUploadAndSaveProtectedAsset?: OnSaveHandler
  hasEditFunction?: boolean
  hasSaveFunction?: boolean
  existingAssetPath: string
  existingAssetUrl: string
  publicAssetUrl: string
  newAssetPath: string
  newAssetFile: File | null
  newTitle: string
}

class EditableAsset extends Component<EditableAssetProps, EditableAssetState> {
  constructor(props) {
    super(props)

    const labels = props.labels || []
    const emptyForm = EMPTY_FORM.set('labels', List())
    this.state = {
      asset: EMPTY_ASSET,
      form: EMPTY_FORM,
      editable: false,
      editing: false,
      mode: null,
      emptyForm,
      emptyState: {},
      labels,
      prefix: props.prefix || '',

      // TODO: Move these to props?
      existingAssetPath: '',
      existingAssetUrl: '',
      publicAssetUrl: '',
      newAssetPath: '',
      newAssetFile: null,
      newTitle: ''
    }
  }

  static defaultCalcDefaultPath = (f: File) => sanitizeFileName(f.name)

  calcDefaultPath =
    this.props.calcDefaultPath ?? EditableAsset.defaultCalcDefaultPath

  static getDerivedStateFromProps(
    nextProps: EditableAssetProps,
    nextState: EditableAssetState
  ) {
    // TODO: Add default save method for updating assets?
    const onSave = isFunction(nextProps.onSave) ? nextProps.onSave : undefined
    const onDelete = isFunction(nextProps.onDelete)
      ? nextProps.onDelete
      : (asset: ImResolvedAsset) => deleteAsset({ id: asset.get('id') })

    const onSavePublicAsset = isFunction(nextProps.onSavePublicAsset)
      ? nextProps.onSavePublicAsset
      : onSave
    const onSaveExistingProtectedAsset = isFunction(
      nextProps.onSaveExistingProtectedAsset
    )
      ? nextProps.onSaveExistingProtectedAsset
      : onSave
    const onUploadAndSaveProtectedAsset = isFunction(
      nextProps.onUploadAndSaveProtectedAsset
    )
      ? nextProps.onUploadAndSaveProtectedAsset
      : onSave

    const afterSave = nextProps.afterSave ?? undefined
    const afterDelete = nextProps.afterDelete ?? undefined

    let asset =
      (nextProps.asset as ImEmptyResolvedAsset) ||
      nextState.asset ||
      EMPTY_ASSET
    if (nextProps.src) {
      asset = asset.set('view_url', nextProps.src)
    }
    const assetId = asset.get('id')
    const hasSaveFunction = List.of(
      onSavePublicAsset,
      onSaveExistingProtectedAsset,
      onUploadAndSaveProtectedAsset,
      onSave
    ).some(isFunction)

    const hasEditFunction = hasSaveFunction || !!onDelete

    const editingDisabled = nextProps.editable === false
    if (editingDisabled && !assetId) {
      throw new Error(
        'Cannot create EditableAsset with editable=false unless an existing asset is given'
      )
    }

    const assetClient = new AssetClient(nextProps.client)

    // Set next state
    const editable = !editingDisabled && hasEditFunction
    const editing = nextState.editing || (editable && !asset.get('id'))
    const mode = nextState.mode || EditableAsset.defaultMode(nextProps)

    // Set defaults
    const labels = nextProps.labels || nextState.labels || []
    const emptyForm = EMPTY_FORM.set('labels', List(labels))
    const form = nextProps.form || nextState.form || emptyForm
    const prefix = nextProps.prefix || nextState.prefix || ''

    const finalState: EditableAssetState = {
      ...nextState,
      asset,
      assetClient,
      editing,
      editable,
      emptyForm,
      emptyState: EditableAsset.getEmptyState(nextProps, nextState),
      form,
      labels,
      mode,
      prefix,
      afterSave,
      afterDelete,
      onDelete,
      onSave,
      onSaveExistingProtectedAsset,
      onSavePublicAsset,
      onUploadAndSaveProtectedAsset,
      hasEditFunction,
      hasSaveFunction
    }

    return finalState
  }

  static getEmptyState = (
    props: EditableAssetProps,
    state: EditableAssetState
  ): Partial<EditableAssetState> => {
    return {
      ...RESET_STATE,
      form: EditableAsset.emptyForm(state),
      mode: EditableAsset.defaultMode(props)
    }
  }

  static emptyForm = (state: EditableAssetState) => {
    const labels = state != null ? state.labels : []
    return EMPTY_FORM.set('labels', List(labels))
  }

  static defaultMode = (props: EditableAssetProps) => {
    return props.defaultMode || Mode.ATTACH_PUBLIC_ASSET
  }

  resetState = (
    merge: Partial<EditableAssetState> = { asset: EMPTY_ASSET }
  ) => {
    const { editable, emptyState } = this.state
    const finalState: Partial<EditableAssetState> = {
      editing: editable && !merge.asset!.get('id'),
      ...emptyState,
      ...merge
    }
    this.setState({ ...this.state, ...finalState })
  }

  defaultAssetPathForFile = file => {
    const defaultPath = file && this.calcDefaultPath(file)
    return defaultPath || ''
  }

  newAssetPathOrDefault = () => {
    return (
      this.state.newAssetPath ||
      this.defaultAssetPathForFile(this.state.newAssetFile)
    )
  }

  /*
   * START: Common Asset Fields
   */

  updateAssetTitle: React.ChangeEventHandler<HTMLInputElement> = e => {
    const newTitle = e.target.value
    this.setState({ newTitle })
  }

  /*
   * START: New Protected Assets
   */

  updateNewProtectedAssetPath: React.ChangeEventHandler<
    HTMLInputElement
  > = e => {
    const newAssetPath = e.target.value
    this.setState({ newAssetPath })
  }

  updateNewProtectedAssetFile: React.ChangeEventHandler<
    HTMLInputElement
  > = e => {
    const newAssetFile = e.target.files && e.target.files[0]
    if (!newAssetFile) {
      return
    }
    const contentType = contentTypeForFile(newAssetFile)
    this.setState(state => ({
      newAssetFile,
      form: state.form.set('content_type', contentType)
    }))
  }

  validateNewProtectedAssetPath = (path: string) => {
    const relativePath = this.expandProtectedAssetPath(path)
    const matchingAsset = this.props.assets
      .get('byId', fromJS<AssetsById>({}))
      .find((a: ImResolvedAsset) => a.get('relative_path') === relativePath)
    return !matchingAsset
  }

  createNewProtectedAsset = async () => {
    const { form, newAssetFile = null, newTitle } = this.state
    const contentType =
      newAssetFile != null ? contentTypeForFile(newAssetFile) : null
    const relativePath = this.expandProtectedAssetPath(
      this.newAssetPathOrDefault()
    )
    const body = form
      .set('asset_type', PRESIGNED_URL)
      .set('relative_path', relativePath)
      .set('content_type', contentType)
      .set('title', newTitle)

    if (
      this.state.onUploadAndSaveProtectedAsset != null &&
      newAssetFile != null
    ) {
      const update = await this.state.onUploadAndSaveProtectedAsset(
        body,
        newAssetFile
      )
      this.resetState({
        asset: (update as ImEmptyResolvedAsset) ?? EMPTY_ASSET
      })

      if (this.state.afterSave != null) {
        this.state.afterSave(body)
      }
    }
  }

  renderNewProtectedAssetUploadBox = () => {
    return (
      this.state.onUploadAndSaveProtectedAsset && (
        <div className="asset-editor asset-new-protected-upload-box">
          <label>
            Upload Content
            <input type="file" onChange={this.updateNewProtectedAssetFile} />
          </label>
        </div>
      )
    )
  }

  renderNewProtectedAssetPathEditor = () => {
    const { newAssetPath, newAssetFile, newTitle } = this.state
    const pathOrDefault = this.newAssetPathOrDefault()
    const isDuplicate = !this.validateNewProtectedAssetPath(pathOrDefault)
    return (
      <div className="asset-editor asset-new-protected-path-editor">
        <p>
          <label>
            Path
            <input
              type="text"
              placeholder={this.defaultAssetPathForFile(newAssetFile)}
              value={newAssetPath}
              onChange={this.updateNewProtectedAssetPath}
            />
          </label>
        </p>
        <p>{this.expandProtectedAssetPath(pathOrDefault)}</p>
        <p>
          <label>
            Title
            <input
              type="text"
              value={newTitle}
              onChange={this.updateNewProtectedAssetPath}
            />
          </label>
        </p>
        <p>
          <button disabled={isDuplicate} onClick={this.createNewProtectedAsset}>
            Attach
          </button>
          <button onClick={this.cancelEdit}>Cancel</button>
        </p>
        {isDuplicate && <div>(path already taken)</div>}
      </div>
    )
  }

  /*
   * Start: Existing Protected Assets
   */

  expandProtectedAssetPath = path => {
    return `${this.state.prefix}${path}`
  }

  updateExistingProtectedAssetPath = e => {
    const existingAssetPath = e.target.value
    this.setState({
      existingAssetPath
    })
  }

  previewExistingProtectedAsset = async () => {
    const { existingAssetPath, form } = this.state
    const fullExistingAssetPath = this.expandProtectedAssetPath(
      existingAssetPath
    )

    if (this.state.assetClient != null) {
      const url = await this.state.assetClient.getViewUrl(fullExistingAssetPath)
      if (url) {
        const contentType = contentTypeForUrl(url)
        const updatedForm = form
          .set('content_type', contentType)
          .set('asset_type', PRESIGNED_URL)
        this.setState({
          form: updatedForm,
          existingAssetUrl: url
        })
      }
    }
  }

  attachExistingProtectedAsset = async () => {
    const { existingAssetPath, existingAssetUrl, form, newTitle } = this.state
    const relativePath = this.expandProtectedAssetPath(existingAssetPath)
    const contentType = contentTypeForUrl(existingAssetUrl)
    const body = form
      .set('asset_type', PRESIGNED_URL)
      .set('content_type', contentType)
      .set('relative_path', relativePath)
      .set('title', newTitle)

    if (this.state.onSaveExistingProtectedAsset != null) {
      const update = await this.state.onSaveExistingProtectedAsset(body)
      this.resetState({
        asset: (update as ImEmptyResolvedAsset) ?? EMPTY_ASSET
      })

      if (this.state.afterSave != null) {
        this.state.afterSave(body)
      }
    } else {
      throw new Error('No existing protected asset save handler defined')
    }
  }

  renderExistingProtectedAssetPathEditor = () => {
    const { existingAssetPath, existingAssetUrl, newTitle } = this.state
    const fullExistingAssetPath = this.expandProtectedAssetPath(
      existingAssetPath || ''
    )
    const actionButton = existingAssetUrl ? (
      <button onClick={this.attachExistingProtectedAsset}>Attach</button>
    ) : (
      <button onClick={this.previewExistingProtectedAsset}>Preview</button>
    )
    return (
      <div className="asset-editor asset-existing-path-editor">
        <p>
          <label>
            Path
            <input
              type="text"
              placeholder={'Relative path in storage'}
              value={existingAssetPath}
              onChange={this.updateExistingProtectedAssetPath}
            />
          </label>
        </p>
        <p>{fullExistingAssetPath}</p>
        <p>
          <label>
            Title
            <input
              type="text"
              placeholder={'Title of content'}
              value={newTitle}
              onChange={this.updateAssetTitle}
            />
          </label>
        </p>
        <p>
          {actionButton}
          {this.showCancelButton() && (
            <button onClick={this.cancelEdit}>Cancel</button>
          )}
        </p>
      </div>
    )
  }

  /*
   * START: Public Assets
   */

  updatePublicAssetUrl = e => {
    const publicAssetUrl = e.target.value
    this.setState({ publicAssetUrl })
  }

  attachPublicAsset = async () => {
    const { publicAssetUrl, form, newTitle } = this.state
    const contentType = contentTypeForUrl(publicAssetUrl)
    const body = form
      .set('asset_type', EXTERNAL_URL)
      .set('external_url', publicAssetUrl)
      .set('content_type', contentType)
      .set('title', newTitle)

    if (this.state.onSavePublicAsset != null) {
      const update = await this.state.onSavePublicAsset(body)
      this.resetState({
        asset: (update as ImEmptyResolvedAsset) ?? EMPTY_ASSET
      })

      if (this.state.afterSave != null) {
        this.state.afterSave(body)
      }
    } else {
      throw new Error('No public asset save handler defined')
    }
  }

  renderPublicAssetUrlEditor = () => {
    const { publicAssetUrl, newTitle } = this.state
    return (
      <div className="asset-editor asset-public-url-editor">
        <p>
          <label>
            URL
            <input
              type="text"
              placeholder="Full url to asset"
              value={publicAssetUrl}
              onChange={this.updatePublicAssetUrl}
            />
          </label>
        </p>
        <p>{publicAssetUrl}</p>
        <p>
          <label>
            Title
            <input
              type="text"
              placeholder={'Title of content'}
              value={newTitle}
              onChange={this.updateAssetTitle}
            />
          </label>
        </p>
        <p>
          <button onClick={this.attachPublicAsset}>Attach</button>
          {this.showCancelButton() && (
            <button onClick={this.cancelEdit}>Cancel</button>
          )}
        </p>
      </div>
    )
  }

  /*
   * END
   */

  cancelEdit = () => {
    this.resetState({ asset: this.state.asset, editing: false })
  }

  delete = () => {
    if (this.showDeleteButton()) {
      const { asset } = this.state
      if (
        confirm(
          `Are you sure you would like to delete asset #${asset.get('id')}`
        ) &&
        this.state.onDelete != null
      ) {
        this.state.onDelete(asset as ImResolvedAsset)

        if (this.state.afterDelete != null) {
          this.state.afterDelete()
        }
      }
    }
  }

  openEditForm = () => {
    this.setState({ editing: true })
  }

  showUploadSourcePicker = () => {
    const { editable, editing, hasSaveFunction } = this.state
    return editable && editing && hasSaveFunction
  }

  showEditButton = () => {
    const { editable, editing } = this.state
    return editable && !editing
  }

  showCancelButton = () => {
    const { asset, editable, editing } = this.state
    return editable && editing && !!asset.get('id')
  }

  showDeleteButton = () => {
    const { asset, editable, editing } = this.state
    return editable && editing && this.state.onDelete && asset.get('id')
  }

  renderTitle() {
    const { asset, newTitle } = this.state
    const currentTitle = asset.get('title')
    const title = currentTitle || newTitle
    return <h4>{title || '(Untitled)'}</h4>
  }

  renderPreviewBar = () => {
    return (
      <div className="asset-preview-bar">
        {this.showEditButton() && (
          <button onClick={this.openEditForm}>Edit</button>
        )}
        {this.showCancelButton() && (
          <button onClick={this.cancelEdit}>Cancel</button>
        )}
        {this.showDeleteButton() && (
          <button onClick={this.delete}>Delete</button>
        )}
      </div>
    )
  }

  renderPreview = () => {
    const {
      asset,
      editing,
      form,
      publicAssetUrl,
      existingAssetUrl,
      mode,
      newAssetFile
    } = this.state
    const currentAssetUrl = asset.get('view_url')
    let previewUrl
    if (editing) {
      switch (mode) {
        case Mode.UPLOAD_PROTECTED_ASSET:
          previewUrl = newAssetFile && URL.createObjectURL(newAssetFile)
          break
        case Mode.ATTACH_PUBLIC_ASSET:
          previewUrl = publicAssetUrl
          break
        case Mode.ATTACH_PROTECTED_ASSET:
          previewUrl = existingAssetUrl
          break
      }
    }
    const displayUrl = previewUrl || currentAssetUrl
    if (displayUrl) {
      const contentType =
        asset.get('content_type') ||
        form.get('content_type') ||
        contentTypeForUrl(displayUrl)
      const relativePath = asset.get('relative_path') || ''
      let view
      switch (contentType) {
        case 'audio':
          view = <audio src={displayUrl} controls />
          break
        case 'image':
          const altTextParts = relativePath.split('/')
          const contentPath = altTextParts.slice(0, 2)
          const fileNameParts = altTextParts.slice(2)
          const altText =
            this.props.alt ||
            [...contentPath, contentType, ...fileNameParts].join(' ')
          view = (
            <img src={displayUrl} alt={altText} style={{ maxWidth: '400px' }} />
          )
          break
        case 'video':
          view = <video controls src={displayUrl} />
          break
        default:
          view = <p>Unrecognized Content</p>
      }
      return <div className="asset-preview-content">{view}</div>
    }
  }

  renderUploadSourcePicker = () => {
    if (!this.showUploadSourcePicker()) {
      return null
    }
    const { mode } = this.state
    return (
      <ToggleButtonGroup
        name="upload-source"
        onChange={(value: ModeType) => {
          this.setState({ editing: true, mode: value })
        }}
        type="radio"
        className="upload-source-picker"
        value={this.state.mode ?? undefined}
      >
        {this.state.onUploadAndSaveProtectedAsset && (
          <ToggleButton value={Mode.UPLOAD_PROTECTED_ASSET}>
            Upload New
          </ToggleButton>
        )}
        {this.state.onSaveExistingProtectedAsset && (
          <ToggleButton value={Mode.ATTACH_PROTECTED_ASSET}>
            Attach Existing
          </ToggleButton>
        )}
        {this.state.onSavePublicAsset && (
          <ToggleButton value={Mode.ATTACH_PUBLIC_ASSET}>
            Attach Public URL
          </ToggleButton>
        )}
      </ToggleButtonGroup>
    )
  }

  renderEditForm = () => {
    const { newAssetFile, mode } = this.state
    if (this.showUploadSourcePicker()) {
      switch (mode) {
        case Mode.ATTACH_PUBLIC_ASSET:
          return this.renderPublicAssetUrlEditor()
        case Mode.ATTACH_PROTECTED_ASSET:
          return this.renderExistingProtectedAssetPathEditor()
        case Mode.UPLOAD_PROTECTED_ASSET:
          return newAssetFile
            ? this.renderNewProtectedAssetPathEditor()
            : this.renderNewProtectedAssetUploadBox()
      }
    }
  }

  render() {
    return (
      <div>
        {this.renderTitle()}
        {this.renderPreviewBar()}
        {this.renderPreview()}
        {this.renderUploadSourcePicker()}
        {this.renderEditForm()}
      </div>
    )
  }
}

export default connect<
  EditableAssetStateProps,
  null,
  EditableAssetOwnProps,
  RootReducer
>(state => state)(EditableAsset)
