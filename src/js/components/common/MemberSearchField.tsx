import React, { Component } from 'react'
import Autosuggest, {
  ChangeEvent,
  SuggestionsFetchRequestedParams,
  SuggestionSelectedEventData
} from 'react-autosuggest'
import {
  Members,
  MemberSearchResponse,
  EligibleMemberList,
  EligibleMemberListEntry
} from '../../client/MemberClient'

const getSuggestionValue = (suggestion: EligibleMemberListEntry) =>
  suggestion.name

interface MemberSearchFieldProps {
  onMemberSelected(member: EligibleMemberListEntry): void
}

interface MemberSearchFieldState {
  suggestions: EligibleMemberList
  searchInput: string
}

export default class MemberSearchField extends Component<
  MemberSearchFieldProps,
  MemberSearchFieldState
> {
  suggestionsCache: { [searchTerm: string]: EligibleMemberList }
  constructor(props) {
    super(props)
    this.suggestionsCache = {}
    this.state = {
      suggestions: [],
      searchInput: ''
    }
  }

  render() {
    const inputProps = {
      placeholder: 'Name or email address',
      className: 'form-control member-search',
      value: this.state.searchInput,
      onChange: (e, args) => this.onChange(e, args),
      id: 'member-search'
    }

    const renderMember = (member, { query, isHighlighted }) => (
      <div className={isHighlighted ? 'suggestion-highlighted' : ''}>
        <a href="javascript:void(0)">{member.name}</a>
      </div>
    )

    return (
      <div>
        <label htmlFor="member-search">
          Search for a member by name or email address
        </label>
        <Autosuggest
          suggestions={this.state.suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          onSuggestionSelected={this.onSuggestionSelected}
          highlightFirstSuggestion={true}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderMember}
          inputProps={inputProps}
        />
      </div>
    )
  }

  onChange = (event: React.FormEvent, args: ChangeEvent) => {
    this.setState({
      searchInput: args.newValue
    })
  }

  onSuggestionsFetchRequested = async (
    event: SuggestionsFetchRequestedParams
  ) => {
    const inputValue = event.value.trim()
    const inputLength = inputValue.length

    if (inputLength === 0) {
      this.onSuggestionsClearRequested()
    } else {
      if (!this.suggestionsCache[inputValue]) {
        const results = await Members.search(inputValue)
        const members = ((results.toJS() as unknown) as MemberSearchResponse)
          .members
        this.suggestionsCache[inputValue] = members
      }
      const suggestions = this.suggestionsCache[inputValue]
      this.setState({
        suggestions
      })
    }
  }

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    })
  }

  onSuggestionSelected = async (
    event: React.FormEvent,
    { suggestion }: SuggestionSelectedEventData<EligibleMemberListEntry>
  ) => {
    const member = suggestion
    this.setState({ searchInput: '' })
    this.props.onMemberSelected(member)
  }
}
