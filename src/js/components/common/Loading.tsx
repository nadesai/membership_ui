import React, { Component } from 'react'
import { Spinner, Container } from 'react-bootstrap'

export default class Loading extends Component {
  render() {
    return (
      <Container className="text-center">
        <h1>Loading...</h1>
        <Spinner variant="danger" animation="border" />
      </Container>
    )
  }
}
