import React, { Component } from 'react'
import { Container } from 'react-bootstrap'
import NotificationSystem, { System } from 'react-notification-system'
import { get as _get } from 'lodash'
import classNames from 'classnames'

import Navigation from './navigation/Navigation'
import { serviceLocator } from '../util/serviceLocator'
import { isFullscreen } from '../util/isFullscreen'
import SkipLink from './common/SkipLink'
import { MAIN_HTML_ID } from '../config'

export default class App extends Component {
  notificationSystemRef = React.createRef<System>()

  componentDidMount() {
    serviceLocator.notificationSystem = this.notificationSystemRef
  }

  render() {
    let navContainer: JSX.Element | null = null
    const pathname = _get(this.props, ['location', 'pathname'], '')

    if (!pathname || !isFullscreen(pathname)) {
      navContainer = (
        <div className="nav-container">
          <Navigation />
        </div>
      )
    }
    return (
      <div
        className={classNames('app-container', {
          fullscreen: isFullscreen(pathname)
        })}
      >
        <SkipLink targetId={MAIN_HTML_ID}>Skip to main content</SkipLink>
        {navContainer}
        <div id={MAIN_HTML_ID} className="content-container" tabIndex={-1}>
          {this.props.children}
        </div>

        <NotificationSystem ref={this.notificationSystemRef} />
      </div>
    )
  }
}
