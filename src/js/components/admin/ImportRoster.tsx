import React, { Component } from 'react'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Container } from 'react-bootstrap'
import { isAdmin } from '../../services/members'
import { importRoster } from '../../redux/actions/memberActions'
import PageHeading from '../common/PageHeading'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'

interface ImportRosterRouteParamProps {}
interface ImportRosterParamProps extends ImportRosterRouteParamProps {}

type ImportRosterProps = ImportRosterStateProps &
  ImportRosterDispatchProps &
  RouteComponentProps<ImportRosterParamProps, ImportRosterRouteParamProps>

interface ImportRosterState {
  file: File | null
}

class ImportRoster extends Component<ImportRosterProps, ImportRosterState> {
  constructor(props) {
    super(props)
    this.state = { file: null }
  }

  onFileChanged: React.ChangeEventHandler<HTMLInputElement> = e => {
    if (e.target.files != null) {
      this.setState({ file: e.target.files[0] })
    }
  }

  render() {
    if (!isAdmin(this.props.member)) {
      return null
    }

    return (
      <Container>
        <PageHeading level={1}>Import Roster</PageHeading>
        <p>
          <label htmlFor="file">
            Upload the chapter roster as a CSV file. Make sure dates are in
            "m/d/yy HH:MM" format.
          </label>
        </p>
        <p>
          <input
            name="file"
            type="file"
            id="file"
            onChange={this.onFileChanged}
          />
        </p>
        <p>
          <button
            type="submit"
            onClick={() => this.submit()}
            disabled={
              this.props.memberImport.get('state') === 'initiated' ||
              this.state.file == null
            }
          >
            Import
          </button>
        </p>
        {this.renderResult()}
      </Container>
    )
  }

  renderResult() {
    switch (this.props.memberImport.get('state')) {
      case 'initiated':
        return <p>Importing...</p>
      case 'success':
        const importResults = this.props.memberImport.get('results')
        if (importResults != null) {
          return (
            <div>
              <p>Import complete!</p>
              <ul>
                <li>
                  {importResults.get('members_created')} members added,{' '}
                  {importResults.get('members_updated')} members updated.
                </li>
                <li>
                  {importResults.get('memberships_created')} memberships added.
                </li>
                <li>
                  {importResults.get('identities_created')} identities added.
                </li>
                <li>
                  {importResults.get('phone_numbers_created')} phone numbers
                  added.
                </li>
                <li>{importResults.get('member_roles_added')} roles added.</li>
                <li>{importResults.get('member_roles_added')} roles added.</li>
              </ul>
            </div>
          )
        } else {
          return <p>Couldn't load imports</p>
        }
      case 'failure':
        return <p>Import failed.</p>
      default:
        return null
    }
  }

  submit() {
    if (this.state.file != null) {
      this.props.importRoster(this.state.file)
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ importRoster }, dispatch)

type ImportRosterStateProps = RootReducer
type ImportRosterDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  ImportRosterStateProps,
  ImportRosterDispatchProps,
  null,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(ImportRoster)
