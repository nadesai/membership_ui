import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Row, Col } from 'react-bootstrap'

import * as Actions from '../../../redux/actions/memberSearchActions'
import { ImMemberSearchState } from '../../../redux/reducers/memberSearchReducer'

import SearchInput from './SearchInput'
import SearchResults from './SearchResults'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'

type MemberSearchProps = MemberSearchDispatchProps & MemberSearchStateProps

class MemberSearch extends React.Component<MemberSearchProps> {
  constructor(props) {
    super(props)
  }
  handleLoadMore = () => {
    const { loadMoreResults, memberSearch } = this.props
    loadMoreResults(memberSearch.get('query'), memberSearch.get('cursor'))
  }
  render() {
    const {
      memberSearch,
      updateMemberSearchInput,
      clearMemberSearchInput,
      searchMembers
    } = this.props
    return (
      <div className="admin-member-search">
        <Row>
          <Col sm={4}>
            <SearchInput
              {...memberSearch.toObject()}
              onChange={updateMemberSearchInput}
              onClear={clearMemberSearchInput}
              onSearch={searchMembers}
            />
          </Col>
        </Row>

        <SearchResults
          {...memberSearch.toObject()}
          loadMore={this.handleLoadMore}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: RootReducer): MemberSearchStateProps => ({
  memberSearch: state.memberSearch
})
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(Actions, dispatch)

type MemberSearchDispatchProps = ReturnType<typeof mapDispatchToProps>

interface MemberSearchStateProps {
  memberSearch: ImMemberSearchState
}

export default connect<MemberSearchStateProps, MemberSearchDispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(MemberSearch)
