import React, { useState, useMemo } from 'react'
import { isAdmin } from '../../services/members'
import { connect } from 'react-redux'
import {
  Container,
  Table,
  InputGroup,
  Form,
  Spinner,
  DropdownButton,
  Dropdown,
  Button,
  OverlayTrigger,
  Popover,
  ButtonGroup
} from 'react-bootstrap'
import { Link } from 'react-router'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import NotFound from 'src/js/components/common/NotFound'
import { USE_ELECTIONS, c } from 'src/js/config'
import {
  ImEligibleMemberList,
  Members,
  EligibleMemberListEntry,
  EligibleMemberList,
  ImMemberSearchResponse,
  ImEligibleMemberListEntry
} from 'src/js/client/MemberClient'
import Loading from 'src/js/components/common/Loading'
import { useQuery } from 'react-query'
import { Column, useTable } from 'react-table'
import { compact, last, fromPairs } from 'lodash/fp'
import { List } from 'immutable'
import { useDebounce } from 'src/js/util/hooks'
import { FiHelpCircle, FiCheckCircle } from 'react-icons/fi'

const PAGE_SIZE = 25

type MemberListStateProps = RootReducer
type MemberListProps = MemberListStateProps

type MembershipSearchOperator = 'none' | 'valid' | 'expired' | 'new'

interface SearchOperators {
  is: string
  membership: MembershipSearchOperator
  location: string
  city: string
  zip: string
}

interface MemberQueryProps {
  query: string
  cursor?: number
}

async function fetchMemberList({
  query,
  cursor
}: MemberQueryProps): Promise<ImMemberSearchResponse> {
  const response = await Members.search(query, PAGE_SIZE, cursor)
  return response
}

const MEMBERSHIP_QUERY_REGEX = /membership:(\w*)|is:(\w*)/

const MemberList: React.FC<MemberListProps> = props => {
  const [query, setQuery] = useState('')
  const debouncedQuery = useDebounce(query, 250)
  const {
    data: pages,
    isLoading,
    error,
    fetchMore,
    canFetchMore,
    isFetchingMore
  } = useQuery(['member-list', { query: debouncedQuery }], fetchMemberList, {
    paginated: true,
    getCanFetchMore: lastPage => lastPage.get('has_more')
  })

  const loadMoreList = async () => {
    const lastPage = last(pages)
    const cursor = lastPage?.get('cursor')

    console.log('Last page cursor:', cursor)

    if (cursor != null) {
      await fetchMore({
        query,
        cursor
      })
    }
  }

  const parseSearchOperators = (query: string): { [key: string]: string } => {
    const matched = query.matchAll(/(\w*):(\w*)/g)
    return fromPairs(
      [...matched].map(([_, operator, qualifier]) => [operator, qualifier])
    )
  }

  const handleToggleMembership = (
    membershipState: MembershipSearchOperator
  ) => e => {
    const operators = parseSearchOperators(query)
    const strippedQuery = query
      .replace(/membership:(\w*)/, '')
      .replace(/is:(\w*)/, '')

    console.log(strippedQuery, operators, 'replace with', membershipState)

    if (
      operators['membership'] === membershipState ||
      operators['is'] === membershipState
    ) {
      // Remove the operator from the query
      setQuery(strippedQuery.trim())
    } else {
      // Add the new operator in the query
      setQuery(`membership:${membershipState} ${strippedQuery}`.trim())
    }
  }

  const handleFetchMore: React.MouseEventHandler<HTMLButtonElement> = () =>
    loadMoreList()

  const allPageMembers =
    pages != null
      ? List<ImEligibleMemberListEntry>().concat(
          ...pages.map(page => page.get('members'))
        )
      : null

  if (!isAdmin(props.member)) {
    return <NotFound />
  }

  return (
    <Container fluid className="member-list-page admin-page">
      <section className="member-list-header">
        <PageHeading level={2}>Members</PageHeading>
        <div className="member-list-controls">
          <InputGroup className="search-controls member-list-control">
            <Form.Control
              type="text"
              value={query}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setQuery(e.target.value)
              }
              placeholder="Search users..."
            />
            <InputGroup.Append
              className={`clear-member-search ${query && 'can-clear'}`}
              onClick={() => setQuery('')}
            >
              <InputGroup.Text>x</InputGroup.Text>
            </InputGroup.Append>
          </InputGroup>
          <DropdownButton
            id="membership-dropdown"
            title="Membership"
            variant="outline-secondary"
            className="member-list-control"
          >
            <Dropdown.Item onClick={handleToggleMembership('valid')}>
              {query.indexOf('membership:valid') >= 0 && <FiCheckCircle />}{' '}
              Active
            </Dropdown.Item>
            <Dropdown.Item onClick={handleToggleMembership('expired')}>
              {query.indexOf('membership:expired') >= 0 && <FiCheckCircle />}{' '}
              Expired
            </Dropdown.Item>
            <Dropdown.Item onClick={handleToggleMembership('new')}>
              {query.indexOf('membership:new') >= 0 && <FiCheckCircle />} New
            </Dropdown.Item>
            <Dropdown.Item onClick={handleToggleMembership('none')}>
              {query.indexOf('membership:none') >= 0 && <FiCheckCircle />}{' '}
              Missing
            </Dropdown.Item>
          </DropdownButton>
          <OverlayTrigger
            trigger="click"
            placement="bottom"
            overlay={
              <Popover id="help-popover">
                <Popover.Title as="h3">Search help</Popover.Title>
                <Popover.Content className="help-popover-content">
                  <section>
                    <p>
                      To search for members by their name or email, just type
                      into the search box.
                    </p>
                    <h4>Membership status</h4>
                    <p>
                      To search for members by membership status, use the
                      Membership dropdown, or type:
                    </p>
                    <ul>
                      <li>
                        <code>membership:valid</code> to find all members with
                        paid dues
                      </li>
                      <li>
                        <code>membership:expired</code> to find members with
                        lapsed dues{' '}
                      </li>
                      <li>
                        <code>membership:new</code> to find members that joined
                        recently (within the past few weeks)
                      </li>
                      <li>
                        <code>membership:none</code> to find users with no
                        membership information
                      </li>
                    </ul>
                    <h4>Location</h4>
                    <p>To search for members by location, type:</p>
                    <ul>
                      <li>
                        <code>location:[city or zip]</code>
                      </li>
                      <li>
                        <code>zip:[zip code]</code>, or
                      </li>
                      <li>
                        <code>city:[city]</code>
                      </li>
                    </ul>
                  </section>
                </Popover.Content>
              </Popover>
            }
          >
            <Button variant="outline-secondary" className="member-list-control">
              <FiHelpCircle /> Help
            </Button>
          </OverlayTrigger>
          <Link to={`/members/create`} className="member-list-control">
            <Button>Add member</Button>
          </Link>
        </div>
      </section>
      <section className="member-list-contents">
        <MemberListTableStack
          members={allPageMembers}
          isLoading={isLoading}
          error={error}
        />
        <div className="member-list-bottom-controls">
          <button
            className="load-more"
            disabled={!canFetchMore}
            onClick={handleFetchMore}
          >
            {isFetchingMore ? <Spinner animation="border" /> : 'Load more...'}
          </button>
        </div>
      </section>
      {pages != null && pages.length > 0 && (
        <section className="member-list-footer">
          <div className="member-list-table-indicators">
            <span className="records-indicator">
              Showing{' '}
              {pages.reduce<number>(
                (previousCount, page) =>
                  page.get('members').size + previousCount,
                0
              )}{' '}
              members
            </span>
          </div>
        </section>
      )}
    </Container>
  )
}

const LoadingMemberListTable: React.FC = () => (
  <section className="loading-member-list non-ideal-member-list">
    <Loading />
  </section>
)

const EmptyMemberListTable: React.FC = () => (
  <section className="empty-member-list non-ideal-member-list">
    <PageHeading level={2}>No members 😿</PageHeading>
    <p>Maybe you should add some!</p>
    <p>
      <Link to={`/members/create`}>
        <button>Add member</button>
      </Link>
      <Link to={`/import`}>
        <button>Import a National DSA roster</button>
      </Link>
    </p>
  </section>
)

const ErrorMemberListTable: React.FC<{ error: Error }> = ({ error }) => (
  <section className="error-member-list non-ideal-member-list">
    <PageHeading level={2}>Error loading members 🆘</PageHeading>
    <p>Try refreshing the page and trying again.</p>
    <p>If that doesn't work, contact {c('ADMIN_EMAIL')}.</p>
    <code>{JSON.stringify(error)}</code>
  </section>
)

interface MemberListTableStackProps {
  members: ImEligibleMemberList | null
  isLoading?: boolean
  error: Error | null
}

const MemberListTableStack: React.FC<MemberListTableStackProps> = ({
  members,
  isLoading,
  error
}) => {
  if (error != null) {
    return <ErrorMemberListTable error={error} />
  } else if (isLoading && (members == null || members.size === 0)) {
    return <LoadingMemberListTable />
  } else if (members == null || members.size === 0) {
    return <EmptyMemberListTable />
  } else {
    return <MemberListTable members={members} />
  }
}

interface TableProps {
  columns: Column<EligibleMemberListEntry>[]
  data: EligibleMemberList
}

const MemberTable: React.FC<TableProps> = ({ columns, data }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow
  } = useTable({
    columns,
    data
  })

  return (
    <Table
      striped
      bordered
      hover
      {...getTableProps()}
      className="member-list-table"
    >
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </Table>
  )
}

interface MemberListTableProps {
  members: ImEligibleMemberList
}

const MemberListTable: React.FC<MemberListTableProps> = ({ members }) => {
  const columns = useMemo<Column<EligibleMemberListEntry>[]>(
    () =>
      compact([
        { Header: 'ID', id: 'id', accessor: member => member.id, width: 80 },
        {
          Header: 'Name',
          id: 'name',
          accessor: member => member.name,
          Cell: ({ row: { original } }) => (
            <Link to={`/members/${original.id}`}>{original.name}</Link>
          )
        },
        USE_ELECTIONS && {
          Header: 'Eligibility',
          id: 'eligibility',
          accessor: member => member.eligibility.is_eligible
        },
        { Header: 'Email', id: 'email', accessor: member => member.email },
        {
          Header: 'Phone numbers',
          id: 'phone_numbers',
          accessor: member => member.phone_numbers || null
        },
        {
          Header: 'Join date',
          id: 'join_date',
          accessor: member => member.membership.join_date,
          Cell: ({
            cell: {
              row: { original }
            }
          }) =>
            original.membership.join_date?.toLocaleDateString() ||
            'No membership'
        },
        {
          Header: 'Dues paid until',
          id: 'dues_paid_until',
          accessor: member => member.membership.dues_paid_until,
          Cell: ({
            cell: {
              row: { original }
            }
          }) =>
            original.membership.dues_paid_until?.toLocaleDateString() || 'N/A'
        }
      ]),
    []
  )

  return <MemberTable columns={columns} data={members.toJS()} />
}

export default connect<MemberListStateProps, null, null, RootReducer>(
  state => state
)(MemberList)
