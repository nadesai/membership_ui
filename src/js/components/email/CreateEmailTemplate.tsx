import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fromJS } from 'immutable'
import { bindActionCreators, Dispatch } from 'redux'
import { saveEmailTemplate } from '../../redux/actions/emailTemplateActions'
import { fetchEmailTopics } from '../../redux/actions/emailTopicActions'
import PageHeading from '../common/PageHeading'
import {
  ImEmailTemplateForm,
  ImEmailTemplateById
} from 'src/js/client/EmailTemplateClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { ImEmailTopic } from 'src/js/client/EmailTopicClient'
import { Container } from 'react-bootstrap'

type CreateEmailTemplateOtherProps = RouteComponentProps<{}, {}>
type CreateEmailTemplateProps = CreateEmailTemplateStateProps &
  CreateEmailTemplateDispatchProps &
  CreateEmailTemplateOtherProps

interface CreateEmailTemplateState {
  template: ImEmailTemplateForm
}

class CreateEmailTemplate extends Component<
  CreateEmailTemplateProps,
  CreateEmailTemplateState
> {
  selectedTopicRef = React.createRef<HTMLSelectElement>()

  constructor(props: CreateEmailTemplateProps) {
    super(props)
    this.state = {
      template: fromJS({
        name: '',
        subject: '',
        body: '',
        topic_id: -1
      })
    }
    this.props.fetchEmailTopics()
  }

  save() {
    const { template } = this.state
    if (
      template.get('name') !== '' &&
      template.get('subject') !== '' &&
      template.get('body') !== ''
    ) {
      this.props.saveEmailTemplate(template)
    }
  }

  handleNameChange: React.ChangeEventHandler<HTMLTextAreaElement> = event => {
    const { template } = this.state
    this.setState({
      template: template.set('name', event.target.value)
    })
  }

  handleSubjectChange: React.ChangeEventHandler<
    HTMLTextAreaElement
  > = event => {
    const { template } = this.state
    this.setState({
      template: template.set('subject', event.target.value)
    })
  }

  handleBodyChange: React.ChangeEventHandler<HTMLTextAreaElement> = event => {
    const { template } = this.state
    this.setState({
      template: template.set('body', event.target.value)
    })
  }

  getSelectedTopic() {
    const select = this.selectedTopicRef.current
    return select?.options[select.selectedIndex].value
  }

  render() {
    const topics = this.getTopics()
    return (
      <Container>
        <PageHeading level={2}>New Template</PageHeading>
        Topic / Recipients:
        <select
          id="topicsSelect"
          ref={this.selectedTopicRef}
          style={{ marginLeft: 10 }}
        >
          {topics}
        </select>
        <br />
        <br />
        Title
        <br />
        <textarea
          name="title"
          rows={1}
          cols={60}
          onChange={this.handleNameChange}
        />
        <br />
        <br />
        Subject
        <br />
        <textarea
          name="subject"
          rows={1}
          cols={60}
          onChange={this.handleSubjectChange}
        />
        <br />
        <br />
        Body
        <br />
        <textarea
          name="body"
          rows={10}
          cols={60}
          onChange={this.handleBodyChange}
        />
        <br />
        <br />
        <button type="button" name="save" onClick={() => this.save()}>
          Save
        </button>
        <br />
        <br />
      </Container>
    )
  }

  getTopics() {
    const topics = this.props.topics.get('byId')
    return topics.valueSeq().map((topic: ImEmailTopic) => {
      return <option value={topic.get('id')}>{topic.get('name')}</option>
    })
  }
}

function getNewId(templates) {
  return templates.maxBy(t => t.get('id') || fromJS({ id: -1 })).get('id')
}

const mapStateToProps = (
  state: RootReducer,
  props: CreateEmailTemplateOtherProps
): CreateEmailTemplateStateProps => {
  if (state.templates.size > 0) {
    const templates = state.templates.get('byId') as ImEmailTemplateById | null // TODO coercion; fix later
    if (
      templates != null &&
      props.location.state != null &&
      templates.size > props.location.state.template_count
    ) {
      const highestId = getNewId(templates)
      const template = templates.get(highestId)
      props.router.replace({
        pathname: `/email/template/${template.get('id')}`,
        state: template
      })
    }
  }
  return state
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchEmailTopics, saveEmailTemplate }, dispatch)

type CreateEmailTemplateStateProps = RootReducer
type CreateEmailTemplateDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CreateEmailTemplateStateProps,
  CreateEmailTemplateDispatchProps,
  CreateEmailTemplateOtherProps,
  RootReducer
>(
  mapStateToProps,
  mapDispatchToProps
)(CreateEmailTemplate)
