import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import {
  fetchEmailTemplate,
  saveEmailTemplate
} from '../../redux/actions/emailTemplateActions'
import { fetchEmailAddresses } from '../../redux/actions/emailAddressActions'
import { fetchEmailTopics } from '../../redux/actions/emailTopicActions'
import { sendEmailTemplate } from '../../redux/actions/emailTopicActions'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import {
  ImEmailTemplateForm,
  ImEmailTemplate
} from 'src/js/client/EmailTemplateClient'
import { ImEmail } from 'src/js/client/EmailAddressClient'
import { ImEmailTopic } from 'src/js/client/EmailTopicClient'
import { Container } from 'react-bootstrap'

interface EmailTemplateDetailsParamProps {
  templateId: string
}
interface EmailTemplateDetailsRouteParamProps {}
type EmailTemplateDetailsProps = EmailTemplateDetailsStateProps &
  EmailTemplateDetailsDispatchProps &
  RouteComponentProps<
    EmailTemplateDetailsParamProps,
    EmailTemplateDetailsRouteParamProps
  >

interface EmailTemplateDetailsState {
  template?: ImEmailTemplate
}

class EmailTemplateDetails extends Component<
  EmailTemplateDetailsProps,
  EmailTemplateDetailsState
> {
  topicsSelectRef = React.createRef<HTMLSelectElement>()
  emailSelectRef = React.createRef<HTMLSelectElement>()

  constructor(props: EmailTemplateDetailsProps) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    this.props.fetchTemplate(parseInt(this.props.params.templateId))
    this.props.fetchEmailAddresses()
    this.props.fetchEmailTopics()
  }

  save() {
    const template = this.state.template

    if (
      template != null &&
      template.get('name') !== '' &&
      template.get('subject') !== '' &&
      template.get('body') !== ''
    ) {
      this.setState({
        template: template.setIn(['topic_id'], this.getSelectedTopicValue())
      })
      this.props.saveTemplate((template as any) as ImEmailTemplateForm)
    }
  }

  send() {
    // consider restricting sending to only saved templates?
    const topic = this.getSelectedTopicValue()
    const template = this.state.template?.get('id')
    const body = this.getSelectedEmailValue()

    // TODO really need to refactor this to follow React conventions
    // of storing data in state instead of reading contents of DOM
    // nodes out of refs
    if (topic != null && template != null && body != null) {
      this.props.sendEmailTemplate(topic, template, body)
    }
  }

  handleNameChange: React.ChangeEventHandler<HTMLTextAreaElement> = event => {
    this.setState({
      template: this.state.template?.set('name', event.target.value)
    })
  }

  handleSubjectChange: React.ChangeEventHandler<
    HTMLTextAreaElement
  > = event => {
    this.setState({
      template: this.state.template?.set('subject', event.target.value)
    })
  }

  handleBodyChange: React.ChangeEventHandler<HTMLTextAreaElement> = event => {
    this.setState({
      template: this.state.template?.set('body', event.target.value)
    })
  }

  getSelectedTopicValue(): number | null {
    const select = this.topicsSelectRef.current
    const value = select?.options[select.selectedIndex].value
    return value != null ? parseInt(value) : null
  }

  getSelectedEmailValue() {
    const select = this.emailSelectRef.current
    const selected = select?.options[select.selectedIndex]
    if (selected != undefined) {
      return selected.textContent
    }
  }

  render() {
    const template = this.state.template
    if (template == null) {
      return <div />
    }
    const modifiedDate = this.getDate(template)
    const emails = this.getEmailOptions()
    const topics = this.getTopics(template)
    return (
      <Container>
        <h2>{template.get('name')}</h2>
        <br />
        <label htmlFor="emailsSelect">Send From:</label>
        <select
          id="emailsSelect"
          ref={this.emailSelectRef}
          style={{ marginLeft: 10 }}
        >
          {emails}
        </select>
        <br />
        <br />
        <label htmlFor="topicsSelect">Topic / Recipients:</label>
        <select
          id="topicsSelect"
          ref={this.topicsSelectRef}
          style={{ marginLeft: 10 }}
        >
          {topics}
        </select>
        <br />
        <br />
        <label htmlFor="title">Title</label>
        <br />
        <textarea
          id="title"
          name="title"
          rows={1}
          cols={20}
          value={template.get('name')}
          onChange={e => this.handleNameChange(e)}
        />
        <br />
        <br />
        <label htmlFor="subject">Subject</label>
        <br />
        <textarea
          id="subject"
          name="subject"
          rows={1}
          cols={20}
          value={template.get('subject')}
          onChange={e => this.handleSubjectChange(e)}
        />
        <br />
        <br />
        <label htmlFor="body">Body</label>
        <br />
        <textarea
          id="body"
          name="body"
          rows={15}
          cols={40}
          value={template.get('body')}
          onChange={e => this.handleBodyChange(e)}
        />
        <br />
        <br />
        Date modified: {modifiedDate}
        <br />
        <br />
        <button type="button" name="save" onClick={() => this.save()}>
          Save
        </button>
        <button
          style={{ marginLeft: '8px' }}
          type="button"
          name="send"
          onClick={() => this.send()}
        >
          Send Email
        </button>
        <br />
        <br />
      </Container>
    )
  }

  getTemplate() {
    if (this.state != null && this.state.template != null) {
      return this.state.template
    } else if (
      this.props.templates != null &&
      this.props.templates.get('template') != null
    ) {
      return this.props.templates.get('template')
    }
  }

  getDate(template: ImEmailTemplate) {
    const updated = template.get('last_updated')
    return new Date(updated).toDateString()
  }

  getEmailOptions() {
    return this.props.emailAddresses
      .get('byId')
      .valueSeq()
      .map((address: ImEmail) => {
        const email = address.get('email_address')
        return <option value={email}>{email}</option>
      })
  }

  getTopics(template: ImEmailTemplate) {
    const topics = this.props.topics.get('byId')
    return topics.valueSeq().map((topic: ImEmailTopic) => {
      const isSelected = topic.get('id') == template.get('topic_id')
      return (
        <option selected={isSelected} value={topic.get('id')}>
          {topic.get('name')}
        </option>
      )
    })
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      saveTemplate: saveEmailTemplate,
      fetchTemplate: fetchEmailTemplate,
      fetchEmailAddresses,
      fetchEmailTopics,
      sendEmailTemplate
    },
    dispatch
  )

type EmailTemplateDetailsStateProps = RootReducer
type EmailTemplateDetailsDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  EmailTemplateDetailsStateProps,
  EmailTemplateDetailsDispatchProps,
  null,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(EmailTemplateDetails)
