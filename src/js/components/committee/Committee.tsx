import React, { Component } from 'react'
import { List, Map, fromJS } from 'immutable'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { adminEmail, eligibilityEmail } from '../../services/emails'
import {
  fetchCommittee,
  markMemberInactive,
  removeAdmin
} from '../../redux/actions/committeeActions'
import { Members, EligibleMemberListEntry } from '../../client/MemberClient'
import MemberSearchField from '../common/MemberSearchField'
import { isAdmin } from '../../services/members'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { FiMinusCircle } from 'react-icons/fi'
import {
  CommitteesById,
  ImCommitteeDetailed
} from 'src/js/client/CommitteeClient'
import { c } from 'src/js/config'
import Loading from 'src/js/components/common/Loading'
import { Container } from 'react-bootstrap'

interface CommitteeParamProps {
  committeeId: string
}
interface CommitteeRouteParamProps {}
type CommitteeOtherProps = RouteComponentProps<
  CommitteeParamProps,
  CommitteeRouteParamProps
>
type CommitteeProps = CommitteeStateProps &
  CommitteeDispatchProps &
  CommitteeOtherProps

class Committee extends Component<CommitteeProps> {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.fetchCommittee(parseInt(this.props.params.committeeId))
  }

  render() {
    // WARNING: THIS TYPE (ImCommitteeDetailed) IS SOMEWHAT INCORRECT
    // SEE CommitteeClient for more information (reducer hydration issues)
    const committee:
      | ImCommitteeDetailed
      | undefined = this.props.committees
      .get('byId', fromJS<CommitteesById>({}))
      .get(parseInt(this.props.params.committeeId))

    if (committee == null) {
      return <Loading />
    }

    const isAdminFlag = isAdmin(this.props.member)
    const admins = committee
      .get('admins', List())
      .sortBy(member => member.get('name'))
      .map(member => (
        <div
          key={`committee-admin-${member.get('id')}`}
          className="committee-member"
        >
          {isAdminFlag && (
            <button
              onClick={e =>
                this.props.removeAdmin(member.get('id'), committee.get('id'))
              }
              aria-label={`Remove admin role for ${c(
                'GROUP_NAME_SINGULAR'
              )} ${committee.get('name')} from member ${member.get('name')}`}
            >
              <FiMinusCircle />
            </button>
          )}
          {member.get('name')}
        </div>
      ))

    const activeMembers = committee
      .get('active', List())
      .sortBy(member => member.get('name'))
      .map(member => (
        <div
          key={`committee-active-${member.get('id')}`}
          className="committee-member"
        >
          <button
            onClick={e =>
              this.props.markMemberInactive(
                member.get('id'),
                committee.get('id')
              )
            }
            aria-label={`Mark member ${member.get('name')} as in active in ${c(
              'GROUP_NAME_SINGULAR'
            )} ${committee.get('name')} from member `}
          >
            <FiMinusCircle />
          </button>
          {member.get('name')}
        </div>
      ))

    return (
      <Container>
        <PageHeading level={2}>{committee.get('name')}</PageHeading>
        <h3>Admins</h3>
        <p>
          This {c('GROUP_NAME_SINGULAR')} has {admins.size} admins. Contact{' '}
          {adminEmail()} to update this list.
        </p>
        {admins}
        <h3>Active Members</h3>
        <p>
          This {c('GROUP_NAME_SINGULAR')} has {activeMembers.size} active
          members. Use the buttons to mark members as inactive, or enter the
          member's name below to mark them as active. If their name is not in
          the membership portal, invite them to{' '}
          <a href={c('URL_CHAPTER_JOIN')}>join DSA</a> and ask them to forward
          their confirmation email to {eligibilityEmail()}.
        </p>
        {activeMembers}
        <h4>Add Active Member</h4>
        <MemberSearchField onMemberSelected={this.onMemberSelected} />
      </Container>
    )
  }

  onMemberSelected = async (member: EligibleMemberListEntry) => {
    const memberId = member.id
    const committeeId = parseInt(this.props.params.committeeId)

    const committee:
      | ImCommitteeDetailed
      | undefined = this.props.committees
      .get('byId', fromJS<CommitteesById>({}))
      .get(committeeId)

    if (committee != null) {
      const existingMember = committee
        .get('members', List())
        .find(member => member.get('id') === memberId)
      if (existingMember === null) {
        await Members.addRole(memberId, 'member', committeeId)
      }
    }
    await Members.addRole(memberId, 'active', committeeId)

    this.props.fetchCommittee(committeeId)
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    { fetchCommittee, markMemberInactive, removeAdmin },
    dispatch
  )

type CommitteeStateProps = RootReducer
type CommitteeDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CommitteeStateProps,
  CommitteeDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(Committee)
