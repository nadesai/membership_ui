import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Container } from 'react-bootstrap'
import { Link, RouteComponentProps } from 'react-router'
import { Map, fromJS, Seq } from 'immutable'
import * as actions from '../../redux/actions/committeeActions'
import {
  isAdmin,
  isCommitteeAdmin,
  isMemberLoaded
} from '../../services/members'
import Loading from '../common/Loading'
import ConfirmCommittee from '../committee/ConfirmCommittee'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { bindActionCreators, Dispatch } from 'redux'
import { FiMail } from 'react-icons/fi'
import { ImCommittee, CommitteesById } from 'src/js/client/CommitteeClient'
import { c } from 'src/js/config'
import { capitalize } from 'lodash/fp'

interface MyCommitteesParamProps {}
interface MyCommitteesRouteParamProps {}

type MyCommitteesOtherProps = RouteComponentProps<
  MyCommitteesParamProps,
  MyCommitteesRouteParamProps
>
type MyCommitteesProps = MyCommitteesStateProps &
  MyCommitteesDispatchProps &
  MyCommitteesOtherProps

class MyCommittees extends Component<MyCommitteesProps> {
  componentDidMount() {
    this.props.fetchCommittees()
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    const adminView =
      isAdmin(this.props.member) || isCommitteeAdmin(this.props.member)

    return (
      <Container>
        <PageHeading level={1}>
          My {capitalize(c('GROUP_NAME_PLURAL'))}
        </PageHeading>
        <h3>Where am I active?</h3>
        {this.renderActiveCommittees()}
        {adminView && (
          <div>
            <h3>Which {c('GROUP_NAME_PLURAL')} can I update?</h3>
            {this.renderAdminCommittees()}
          </div>
        )}
        <h3>How can I get involved?</h3>
        {this.renderAllCommittees()}
      </Container>
    )
  }

  renderAdminCommittees() {
    const committeeAdminId = this.props.member
      .getIn(['user', 'data', 'roles'])
      .filter(role => role.get('role') === 'admin')
      .filter(role => role.get('committee') !== 'general')
      .map(role => role.get('committee_id'))
    const committees = this.props.committees
      .get('byId', fromJS<CommitteesById>({}))
      .valueSeq()
      .sortBy((committee: ImCommittee) => committee.get('name'))
      .filter(
        (committee: ImCommittee) =>
          isAdmin(this.props.member) ||
          committeeAdminId.includes(committee.get('id'))
      ) as Seq.Indexed<ImCommittee>

    if (isAdmin(this.props.member)) {
      return (
        <div>
          <p>You can update any {c('GROUP_NAME_SINGULAR')}:</p>
          {committees.map(committee =>
            this.renderCommittee(committee, true, false)
          )}
        </div>
      )
    } else {
      return (
        <p>
          You can update{' '}
          {committees
            .map(committee => (
              <Link to={`/committees/${committee.get('id')}`}>
                <strong>{committee.get('name')}</strong>
              </Link>
            ))
            .interpose(<span className="separator">, </span>)}
          .
        </p>
      )
    }
  }

  renderActiveCommittees(): JSX.Element {
    const roles = this.props.member.getIn(['user', 'data', 'roles'])
    const committees = roles
      .filter(role => role.get('role') === 'active')
      .map(role => <strong>{role.get('committee_name')}</strong>)
      .filter(name => name !== 'general')

    const currentlyActive =
      committees.size === 0 ? (
        <p>You're currently not active in any {c('GROUP_NAME_PLURAL')}.</p>
      ) : (
        <p>
          You're currently active in {committees.size}{' '}
          {committees.size === 1
            ? c('GROUP_NAME_SINGULAR')
            : c('GROUP_NAME_PLURAL')}
          : {committees.interpose(', ')}.
        </p>
      )

    return (
      <div>
        {currentlyActive}
        <p>
          If you've active been active in a {c('GROUP_NAME_SINGULAR')} but don't
          see it listed, send a request to the co-chairs to have them confirm:
        </p>
        <Container>
          <Col xs={12} sm={8} md={6} lg={4}>
            <ConfirmCommittee />
          </Col>
        </Container>
      </div>
    )
  }

  renderAllCommittees(): JSX.Element {
    const committees = this.props.committees
      .get('byId', fromJS<CommitteesById>({}))
      .valueSeq()
      .sortBy((committee: ImCommittee) => committee.get('name'))
      .map<JSX.Element>((committee: ImCommittee) =>
        this.renderCommittee(committee, false, true)
      )

    return (
      <div>
        <p>
          Our chapter has numerous {c('GROUP_NAME_PLURAL')} focused on various
          types of work, both internal and external. To find out more about what
          they do,{' '}
          <a href={c('URL_CHAPTER_GROUP_INFO')}>click here for more info</a>.
        </p>
        <p>
          If you'd like to get in touch with the leadership for a{' '}
          {c('GROUP_NAME_SINGULAR')}, use the list below:
        </p>
        {committees}
      </div>
    )
  }

  renderCommittee(committee, showLink, showEmailButton) {
    const id = committee.get('id')
    const name = committee.get('name')
    const email = committee.get('email')

    const emailButton =
      showEmailButton && email ? (
        <a href={`mailto:${email}`}>
          <button aria-label={`Send email to ${name}`}>
            <FiMail />
          </button>
        </a>
      ) : null

    const committeeText = showLink ? (
      <Link to={`/committees/${committee.get('id')}`}>{name}</Link>
    ) : (
      name
    )

    return (
      <p key={`committee-${id}`}>
        {emailButton}
        {committeeText}
      </p>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type MyCommitteesStateProps = RootReducer
type MyCommitteesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MyCommitteesStateProps,
  MyCommitteesDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MyCommittees)
