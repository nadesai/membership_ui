import React, { Component } from 'react'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Form } from 'react-bootstrap'
import { Map, fromJS } from 'immutable'
import * as actions from '../../redux/actions/committeeActions'
import FieldGroup from '../common/FieldGroup'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { CommitteesById } from 'src/js/client/CommitteeClient'
import { ImCommittee } from 'src/js/redux/reducers/committeeReducers'
import { capitalize } from 'lodash/fp'
import { c } from 'src/js/config'

type ConfirmCommitteeProps = ConfirmCommitteeStateProps &
  ConfirmCommitteeDispatchProps

interface ConfirmCommitteeState {
  committeeId: number | null
}

class ConfirmCommittee extends Component<
  ConfirmCommitteeProps,
  ConfirmCommitteeState
> {
  constructor(props) {
    super(props)
    this.state = { committeeId: null }
  }

  componentDidMount() {
    this.props.fetchCommittees()
  }

  updateCommitteeId = (formKey: 'committeeId', value: string) => {
    const parsed = parseInt(value)
    if (parsed < 0) {
      this.setState({ committeeId: null })
    } else {
      this.setState({ committeeId: parsed })
    }
  }

  render() {
    const memberCommittees = this.props.member
      .getIn(['user', 'data', 'roles'])
      .filter(role => role.get('role') === 'active')
      .map(role => role.get('committee'))
      .filter(committee => committee !== 'general')

    const committeeIdsByName = this.props.committees
      .get('byId', fromJS<CommitteesById>({}))
      .mapEntries<number, string>(([cid, c]: [number, ImCommittee]) => [
        cid,
        c.get('name')
      ])
      .filter((_, c) => !memberCommittees.includes(c))
      .sort()

    return (
      <Form>
        <FieldGroup
          formKey="committeeId"
          componentClass="select"
          options={committeeIdsByName.toObject()}
          label={capitalize(c('GROUP_NAME_SINGULAR'))}
          onFormValueChange={this.updateCommitteeId}
          value={this.state.committeeId}
          placeholder={`Select a ${c('GROUP_NAME_SINGULAR')}`}
        />
        <Form.Row>
          <button
            type="button"
            onClick={() =>
              this.state.committeeId != null
                ? this.props.requestCommitteeMembership(this.state.committeeId)
                : null
            }
            disabled={this.state.committeeId == null}
          >
            Request confirmation
          </button>
        </Form.Row>
      </Form>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type ConfirmCommitteeStateProps = RootReducer
type ConfirmCommitteeDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  ConfirmCommitteeStateProps,
  ConfirmCommitteeDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(ConfirmCommittee)
