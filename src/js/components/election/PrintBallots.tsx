import React, { Component } from 'react'
import { connect } from 'react-redux'
import { HTTP_POST, logError } from '../../util/util'
import { Button, Form, FormControl, Container } from 'react-bootstrap'
import PaperBallot from './PaperBallot'
import { membershipApi } from '../../services/membership'
import { fromJS, List } from 'immutable'
import {
  Elections,
  ImElectionDetailsResponse
} from '../../client/ElectionClient'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { sanitizeIntermediateBallotInput } from 'src/js/util/ballotKey'

type PrintBallotsStateProps = RootReducer
interface PrintBallotsParamProps {
  electionId: string
}
interface PrintBallotsRouteParamProps {}
type PrintBallotsProps = PrintBallotsStateProps &
  RouteComponentProps<PrintBallotsParamProps, PrintBallotsRouteParamProps>

interface PrintBallotsState {
  numBallotsInput: string
  claimedBallots: List<string>
  readyToPrint: boolean
  claimAndPrintForm: {
    election_id: number
    number_ballots: number
  }
  election: ImElectionDetailsResponse | null
  inSubmission: boolean
}

class PrintBallots extends Component<PrintBallotsProps, PrintBallotsState> {
  constructor(props) {
    super(props)
    this.state = {
      numBallotsInput: '',
      claimedBallots: List(),
      readyToPrint: false,
      claimAndPrintForm: {
        election_id: props.params.electionId,
        number_ballots: 0
      },
      election: null,
      inSubmission: false
    }
  }

  componentWillMount() {
    Elections.getElection(parseInt(this.props.params.electionId)).then(
      results => {
        this.setState({ election: results })
      }
    )
  }

  render() {
    if (this.state.election == null) {
      return null
    }

    const ballots = this.state.claimedBallots.map(ballotKey => (
      <PaperBallot
        key={ballotKey}
        election={this.state.election!}
        ballotKey={ballotKey}
      />
    ))
    return (
      <Container>
        <div className="print-hidden">
          <PageHeading level={1}>
            Print Ballots for {this.state.election.get('name')}
          </PageHeading>
          <Form>
            <label htmlFor="number_ballots">Number of ballots</label>
            <FormControl
              required
              id="number_ballots"
              type="number"
              maxLength={5}
              onChange={e => {
                const numBallotsInput = sanitizeIntermediateBallotInput(
                  e.target.value
                )
                if (numBallotsInput) {
                  const form = this.state.claimAndPrintForm
                  form.number_ballots = parseInt(numBallotsInput, 10)
                  this.setState({
                    claimAndPrintForm: form,
                    numBallotsInput: numBallotsInput
                  })
                } else if (e.target.value.trim() === '') {
                  this.setState({ numBallotsInput: '' })
                }
              }}
              value={this.state.numBallotsInput}
            />
            <button
              type="submit"
              disabled={!this.state.numBallotsInput}
              onClick={e => {
                this.submitForm(e, 'claimAndPrintForm', '/ballot/claim').then(
                  ballotKeys => {
                    this.setState({
                      claimedBallots: fromJS(ballotKeys),
                      readyToPrint: true
                    })
                  }
                )
              }}
            >
              Claim and Print
            </button>
          </Form>
        </div>
        <div id="paper-ballots">{ballots}</div>
      </Container>
    )
  }

  componentDidUpdate() {
    if (this.state.readyToPrint) {
      this.setState({ readyToPrint: false })
      window.print()
    }
  }

  async submitForm(e, formName, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      return await membershipApi(HTTP_POST, endpoint, this.state[formName])
    } catch (err) {
      return logError(`Error submitting form to ${endpoint}`, err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<PrintBallotsStateProps, null, {}, RootReducer>(
  state => state
)(PrintBallots)
