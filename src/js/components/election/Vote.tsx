import React, { Component } from 'react'
import { connect } from 'react-redux'
import { logError } from '../../util/util'
import { Button, ButtonGroup, Form, Container } from 'react-bootstrap'
import { fromJS, is, Map, List } from 'immutable'
import {
  Elections,
  ImElectionDetailsResponse,
  ImSubmitVoteResponse
} from '../../client/ElectionClient'
import { ImCandidate } from 'src/js/client/CandidateClient'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { FromJS } from 'src/js/util/typedMap'
import { shuffle } from 'lodash/fp'

type VoteStateProps = RootReducer
interface VoteParamsProps {
  electionId: number
}
interface VoteRouteParamsProps {}
type VoteProps = VoteStateProps &
  RouteComponentProps<VoteParamsProps, VoteRouteParamsProps>

interface VoteHighlight {
  idx: number
  type: 'anchor' | 'candidate'
}
type ImVoteHighlight = FromJS<VoteHighlight>

interface VoteState {
  election: ImElectionDetailsResponse | null
  unranked: List<ImCandidate>
  ranked: List<ImCandidate>
  highlighted: ImVoteHighlight | null
  inSubmission: boolean
  result: ImSubmitVoteResponse | null
}

class Vote extends Component<VoteProps, VoteState> {
  constructor(props) {
    super(props)
    this.state = {
      election: null,
      unranked: List(),
      ranked: List(),
      highlighted: null,
      inSubmission: false,
      result: null
    }
  }

  componentDidMount() {
    this.getElectionDetails()
  }

  insertAtAnchor({ anchorIdx, candidate, ranked = this.state.ranked }) {
    const updatedRankings = ranked.insert(anchorIdx, candidate)
    this.setState({
      ranked: updatedRankings,
      highlighted: null // undo highlighting since either anchor or candidate was highlighted
    })
  }

  selectRanked(selected: ImVoteHighlight) {
    const highlighted = this.state.highlighted
    if (selected && highlighted) {
      const ranked = this.state.ranked
      const reinsertAtAnchor = ({ anchorIdx, candidateIdx }) => {
        // skip anchors that are above or below the candidate, since they won't move
        if (candidateIdx === anchorIdx || candidateIdx === anchorIdx - 1) {
          this.setState({ highlighted: null })
        } else {
          this.insertAtAnchor({
            // move the anchor up one if it comes after the candidate being moved
            anchorIdx: anchorIdx > candidateIdx ? anchorIdx - 1 : anchorIdx,
            candidate: ranked.get(candidateIdx),
            ranked: ranked.remove(candidateIdx)
          })
        }
      }
      switch (highlighted.get('type')) {
        case 'anchor':
          switch (selected.get('type')) {
            case 'anchor':
              // swap highlighted anchors or toggle anchor highlighting if clicked twice
              this.setState({
                highlighted: is(highlighted, selected) ? null : selected
              })
              break
            case 'candidate':
              // reinsert ranked candidate at highlighted anchor
              reinsertAtAnchor({
                anchorIdx: highlighted.get('idx'),
                candidateIdx: selected.get('idx')
              })
              break
          }
          break
        case 'candidate':
          const highlightedCandidateIdx = highlighted.get('idx')
          const highlightedCandidate = ranked.get(highlightedCandidateIdx)

          if (highlightedCandidate != null) {
            switch (selected.get('type')) {
              case 'anchor':
                // reinsert highlighted ranked candidate at anchor
                reinsertAtAnchor({
                  anchorIdx: selected.get('idx'),
                  candidateIdx: highlightedCandidateIdx
                })
                break
              case 'candidate':
                // swap both ranked candidates
                const candidateArray = ranked.toArray()
                const secondCandidateIdx = selected.get('idx')
                const candidateClicked = candidateArray[secondCandidateIdx]
                candidateArray[highlightedCandidateIdx] = candidateClicked
                candidateArray[secondCandidateIdx] = highlightedCandidate
                const updatedRankings = List(candidateArray)
                this.setState({
                  ranked: updatedRankings,
                  highlighted: null
                })
                break
            }
          } else {
            throw new Error(
              'Bad highlighted candidate when swapping candidates'
            )
          }
          break
      }
    } else if (selected) {
      this.setState({ highlighted: selected })
    }
  }

  addToRanked(unrankedIdx: number) {
    let anchorIdx: number | null = null
    if (this.state.highlighted) {
      const { type, idx } = this.state.highlighted.toObject()
      if (type === 'anchor') {
        anchorIdx = idx
      }
    }
    const candidate = this.state.unranked.get(unrankedIdx)
    if (anchorIdx !== null) {
      this.insertAtAnchor({ anchorIdx, candidate })
    } else if (candidate != null) {
      this.setState({
        ranked: this.state.ranked.push(candidate)
      })
    } else {
      throw new Error('Bad index when ranking candidate')
    }
    this.setState({
      unranked: this.state.unranked.remove(unrankedIdx)
    })
  }

  removeFromRanked(rankedIdx: number) {
    const candidate = this.state.ranked.get(rankedIdx)

    if (candidate != null) {
      this.setState({
        unranked: this.state.unranked.push(candidate),
        ranked: this.state.ranked.remove(rankedIdx)
      })
    } else {
      throw new Error('Bad index when removing ranking')
    }
  }

  render() {
    return this.renderForMobile()
  }

  renderForMobile() {
    if (this.state.election == null) {
      return null
    }

    const highlighted = this.state.highlighted || Map()
    const ranked = this.state.ranked
    const rankedCandidates = ranked.map((candidate, idx) => {
      const current: ImVoteHighlight = fromJS<VoteHighlight>({
        type: 'candidate',
        idx
      })
      return (
        <div key={`ranked-${idx}`}>
          <div style={{ display: 'flex' }}>
            {Vote.candidateImage(candidate)}
            <ButtonGroup size="lg" className="justify-content-between">
              <a
                id={`swap-candidate-${candidate.get('id')}`}
                className={`btn btn-success ${
                  is(current, highlighted) ? 'active hover' : ''
                }`}
                onClick={e => {
                  this.selectRanked(current)
                }}
              >
                {candidate.get('name')}
              </a>
              <a
                className="btn btn-danger"
                id={`drop-candidate-${candidate.get('id')}`}
                onClick={e => {
                  this.removeFromRanked(idx)
                }}
              >
                Unrank
              </a>
            </ButtonGroup>
          </div>
        </div>
      )
    })
    const anchor = (idx: number) => {
      const current: ImVoteHighlight = fromJS<VoteHighlight>({
        type: 'anchor',
        idx
      })
      return (
        <div
          key={`anchor-${idx}`}
          className={`anchor ${is(current, highlighted) ? 'highlighted' : ''}`}
          onClick={e => this.selectRanked(current)}
        >
          <hr />
        </div>
      )
    }
    const rankedCandidatesAndAnchors = rankedCandidates
      .interleave(ranked.map((_, idx) => anchor(idx + 1)))
      .unshift(anchor(0))
    const rankedList = (
      <div id="ranked-candidates">{rankedCandidatesAndAnchors}</div>
    )
    const unrankedList = (
      <div id="unranked-candidates">
        {this.state.unranked.map((candidate, index) => (
          <div key={`unranked-${index}`}>
            <div style={{ display: 'flex' }}>
              {Vote.candidateImage(candidate)}
              <Button block size="lg" onClick={e => this.addToRanked(index)}>
                {candidate.get('name')}
              </Button>
            </div>
          </div>
        ))}
      </div>
    )
    return (
      <Container>
        <PageHeading level={1}>{this.state.election.get('name')}</PageHeading>
        <ul>
          <li>Click unranked candidates to add them in order</li>
          <li>Swap 2 ranked candidates by clicking on their names</li>
          <li>Rank as many as you wish</li>
        </ul>
        <Form onSubmit={e => e.stopPropagation()}>
          <div className="text-center">
            <strong>Ranked</strong>
          </div>
          {rankedList}
          <div className="text-center">
            <strong>Unranked</strong>
          </div>
          {unrankedList}
          {this.state.result == null ? (
            <button
              id="vote"
              disabled={this.state.inSubmission}
              onClick={e => {
                e.preventDefault()
                return this.vote()
              }}
            >
              VOTE
            </button>
          ) : (
            <p>
              Congratulations. You've successfully voted. Your confirmation
              number is
              <strong> {this.state.result.get('ballot_id')}</strong>. Save this
              number if you'd like to confirm your vote was counted correctly
              after the election, but keep it private or everybody will be able
              to see how you voted.
            </p>
          )}
        </Form>
        <div className="footer-padding">
          {/* A fix for some mobile browsers not being able to click the submit button */}
        </div>
      </Container>
    )
  }

  static candidateImage(candidate) {
    const imageUrl = candidate.get('image_url')

    if (imageUrl) {
      return (
        <img
          style={{
            height: 50,
            width: 'auto',
            marginRight: 20
          }}
          src={imageUrl}
          alt={candidate.get('name')}
        />
      )
    } else {
      return null
    }
  }

  async getElectionDetails() {
    try {
      const response = await Elections.getElection(this.props.params.electionId)
      if (response == null) {
        throw new Error("Couldn't load election")
      }
      this.setState({
        election: response,
        unranked: fromJS(shuffle(response.get('candidates').toArray()))
      })
    } catch (err) {
      return logError('Error loading election', err)
    }
  }

  async vote() {
    if (
      !confirm(
        'Are you sure you want to submit your vote now? This cannot be undone.'
      )
    ) {
      return
    }
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    const params = fromJS({
      election_id: this.props.params.electionId,
      rankings: this.state.ranked.map(c => c.get('id'))
    })
    try {
      const result = await Elections.submitVote(params)
      this.setState({ result: result })
    } catch (err) {
      return logError('Error submitting vote', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<VoteStateProps, null, {}, RootReducer>(state => state)(
  Vote
)
