import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isAdmin } from '../../services/members'
import { Elections, ImElectionCount } from '../../client/ElectionClient'
import { logError } from '../../util/util'
import { fromJS, List, Map } from 'immutable'
import { ElectionDetailParamProps } from 'src/js/components/election/ElectionDetail'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import Loading from '../common/Loading'

interface ElectionResultsOwnProps {
  params: ElectionDetailParamProps
}
type ElectionResultsStateProps = RootReducer
type ElectionResultsProps = ElectionResultsStateProps & ElectionResultsOwnProps

interface ElectionResultsState {
  inSubmission: boolean
  results: ImElectionCount | null
}

class ElectionResults extends Component<
  ElectionResultsProps,
  ElectionResultsState
> {
  constructor(props: ElectionResultsProps) {
    super(props)
    this.state = {
      inSubmission: false,
      results: null
    }
  }

  render() {
    const admin = isAdmin(this.props.member)
    const winners = this.state.results
      ?.get('winners', List())
      .map((winner, index) => {
        const candidateID = winner.get('candidate')

        if (this.state.results != null) {
          const candidateDetails = this.state.results
            .get('candidates')
            .get(candidateID.toString())

          if (candidateDetails != null) {
            return <div key={index}>{candidateDetails.get('name')}</div>
          } else {
            return <div key={index}>Couldn't show winning candidate</div>
          }
        } else {
          return <div key={index}>No results</div>
        }
      })

    return admin ? (
      <div>
        <div>
          <button type="submit" onClick={this.countVotes}>
            Count the Vote
          </button>
        </div>
        {this.state.inSubmission && <Loading />}
        {this.state.results != null && this.state.results.size > 0 && (
          <div>
            <h3>Winners</h3>
            {winners}
            <h3>Counts</h3>
            <div>{this.state.results.get('ballot_count')} ballots cast</div>
          </div>
        )}
      </div>
    ) : (
      <div />
    )
  }

  countVotes = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const results = await Elections.countResults(
        parseInt(this.props.params.electionId)
      )
      this.setState({ results })
    } catch (err) {
      return logError('Error counting votes', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<
  ElectionResultsStateProps,
  null,
  ElectionResultsOwnProps,
  RootReducer
>(state => state)(ElectionResults)
