import React, { Component } from 'react'
import { connect } from 'react-redux'
import { membershipApi } from '../../services/membership'
import FieldGroup from '../common/FieldGroup'
import { HTTP_GET, HTTP_POST, logError } from '../../util/util'
import { Button, Col, Row } from 'react-bootstrap'
import { fromJS, Map, List } from 'immutable'
import { isAdmin } from '../../services/members'
import _get from 'lodash/get'
import PageHeading from '../common/PageHeading'
import {
  ImElectionDetailsResponse,
  ImEligibleVotersById,
  EligibleVoter,
  ImEligibleVoter
} from 'src/js/client/ElectionClient'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import Loading from '../common/Loading'

type SignInKioskStateProps = RootReducer
interface SignInKioskParamProps {
  electionId: string
}
interface SignInKioskRouteParamProps {}
type SignInKioskProps = SignInKioskStateProps &
  RouteComponentProps<SignInKioskParamProps, SignInKioskRouteParamProps>

interface SignInKioskState {
  election: ImElectionDetailsResponse | null
  lastResult: string
  voters: ImEligibleVotersById | null
  memberId: string
  searchString: string
  inSubmission: boolean
}

class SignInKiosk extends Component<SignInKioskProps, SignInKioskState> {
  constructor(props) {
    super(props)
    this.state = {
      election: null,
      lastResult: '',
      voters: null,
      memberId: '',
      searchString: '',
      inSubmission: false
    }
  }

  componentDidMount() {
    this.getElectionDetails()
    this.getVoterList()
  }

  updateSearch(value) {
    if (this.state.inSubmission) {
      return
    }
    this.setState({ searchString: value, memberId: '' })
  }

  select(memberId) {
    if (this.state.voters == null) {
      return
    }

    this.setState({
      memberId: memberId,
      searchString: this.state.voters.get(memberId).get('name')
    })
  }

  render() {
    const memberData = this.props.member.getIn(['user', 'data'], null)
    if (this.state.election == null || this.state.voters == null) {
      return <Loading />
    }
    if (!isAdmin(this.props.member)) {
      return <div>This page is for admins only.</div>
    }
    let options: JSX.Element[] | null = null
    if (this.state.searchString.length > 2 && this.state.memberId === '') {
      const regex = new RegExp(this.state.searchString, 'i')
      options = this.state.voters
        .filter((v: ImEligibleVoter) => v.get('name').match(regex))
        .map((v, k) => (
          <Row key={k}>
            <Col sm={{ span: 9, offset: 3 }}>
              <button onClick={event => this.select(k)}>{v}</button>
            </Col>
          </Row>
        ))
        .valueSeq()
        .toArray()
    }
    return (
      <div>
        <PageHeading level={2}> Election </PageHeading>
        <h3> {this.state.election.get('name')} </h3>
        <p>{this.state.lastResult}</p>
        <Col sm={6}>
          <Row>
            <FieldGroup
              label="Member"
              formKey="member_id"
              componentClass="input"
              type="text"
              value={this.state.searchString}
              onFormValueChange={(formKey, value) => this.updateSearch(value)}
            />
          </Row>
          {options}
          <button type="submit" onClick={e => this.issueBallot(e)}>
            Issue Ballot
          </button>
        </Col>
      </div>
    )
  }

  async getElectionDetails() {
    try {
      const results = await membershipApi(HTTP_GET, `/election`, {
        id: this.props.params.electionId
      })
      this.setState({ election: fromJS(results) })
    } catch (err) {
      return logError('Error loading election details', err)
    }
  }

  async getVoterList() {
    try {
      const results = await membershipApi(HTTP_GET, '/election/eligible/list', {
        election_id: this.props.params.electionId
      })
      this.setState({
        voters: fromJS(results).map(
          v => `${v.get('name')} <${v.get('email_address')}>`
        )
      })
    } catch (err) {
      return logError('Error loading eligible voters', err)
    }
  }

  async issueBallot(e) {
    e.preventDefault()
    if (
      this.state.inSubmission ||
      this.state.memberId === '' ||
      this.state.voters == null
    ) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await membershipApi(HTTP_POST, '/ballot/issue', {
        election_id: this.props.params.electionId,
        member_id: this.state.memberId
      })
      const lastResult = `${this.state.voters.get(
        this.state.memberId
      )} was issued a ballot`
      this.setState({ lastResult: lastResult, memberId: '', searchString: '' })
    } catch (err) {
      const errorMessage = _get(
        err,
        ['response', 'body', 'err'],
        err.toString()
      )
      const lastResult = `${this.state.voters.get(
        this.state.memberId
      )} was not issued a ballot because ${errorMessage}`
      this.setState({ lastResult: lastResult })
      alert(lastResult)
      return logError('Error submitting ballot', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<SignInKioskStateProps, null, {}, RootReducer>(
  state => state
)(SignInKiosk)
