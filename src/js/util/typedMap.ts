/* eslint-disable @typescript-eslint/no-explicit-any */
// Help from https://blog.mayflower.de/6630-typescript-redux-immutablejs.html

import { Map, List, Collection } from 'immutable'

type AllowedValue =
  | string
  | number
  | boolean
  | AllowedMap
  | AllowedList
  | TypedMap<any>
  | null
  | undefined

interface AllowedList extends List<AllowedValue> {}

interface AllowedMap extends Map<string, AllowedValue> {}

export type MapTypeAllowedData<DataType> = {
  [K in keyof DataType]: AllowedValue
}

export type ImmutableKeyValue<T> = { [K in keyof T]: FromJS<T[K]> }

export interface TypedMap<DataType extends MapTypeAllowedData<DataType>>
  extends Map<keyof DataType, AllowedValue> {
  toJS(): DataType
  get<K extends keyof DataType>(key: K, notSetValue?: DataType[K]): DataType[K]
  set<K extends keyof DataType>(key: K, value: DataType[K]): this
  toObject(): DataType
}

const createTypedMap = <DataType extends MapTypeAllowedData<DataType>>(
  data: DataType
): TypedMap<DataType> => (Map(data) as any) as TypedMap<DataType>

export const typedMerge = <DataType extends MapTypeAllowedData<DataType>>(
  destination: TypedMap<DataType>,
  ...iterables: Partial<DataType>[]
): TypedMap<DataType> =>
  destination.merge(...(iterables as any)) as TypedMap<DataType>

export const typedMergeDeep = <DataType extends MapTypeAllowedData<DataType>>(
  destination: TypedMap<DataType>,
  ...iterables: Partial<DataType>[]
): TypedMap<DataType> =>
  destination.mergeDeep(...(iterables as any)) as TypedMap<DataType>

export const typedMergeDeepIn = <DataType extends MapTypeAllowedData<DataType>>(
  destination: TypedMap<DataType>,
  keyPath: string[],
  ...iterables: Partial<DataType>[]
): TypedMap<DataType> =>
  destination.mergeDeepIn(keyPath, ...(iterables as any)) as TypedMap<DataType>

/**
 * Adapted from https://github.com/immutable-js/immutable-js/pull/1617
 */
declare module 'immutable' {
  export function fromJS(
    jsValue: any,
    reviver: (
      key: string | number,
      sequence: Collection.Keyed<string, any> | Collection.Indexed<any>,
      path?: Array<string | number>
    ) => any
  ): any

  export function fromJS<JSValue>(
    jsValue: JSValue,
    reviver?: undefined
  ): FromJS<JSValue>
}

export type FromJS<JSValue> = {
  noTransform: JSValue
  array: FromJSArray<JSValue>
  object: FromJSObject<JSValue>
  numberKeyedObject: FromJSNKObject<JSValue>
  any: any
}[JSValue extends NoTransform
  ? 'noTransform'
  : JSValue extends any[]
  ? 'array'
  : JSValue extends { [key: string]: any }
  ? 'object'
  : 'any']

export type NoTransform =
  | ImmutableType
  | Date
  | number
  | string
  | null
  | undefined

export type FromJSArray<JSValue> = JSValue extends Array<infer T>
  ? List<FromJS<T>>
  : never

export type FromJSObject<JSValue> = JSValue extends { [key: string]: any }
  ? TypedMap<ImmutableKeyValue<JSValue>>
  : never

export type FromJSNKObject<JSValue> = JSValue extends {
  [key: number]: infer TValue
}
  ? Map<number, FromJS<TValue>>
  : never

export type ImmutableType = Collection<any, any>

export function typedKeyBy<T extends ImmutableType, U>(
  collection: List<T>,
  selectorFn: (item: T) => U
): Map<U, T> {
  return Map(collection.map(item => [selectorFn(item), item]))
}
